package com.bydrone.droneapplication;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.navigation.NavArgument;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;

import com.bydrone.droneapplication.Utils.StringConstants;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

/**
 * @author Ankur Khandelwal on 2019-08-23.
 */
public class MainActivity extends BaseActivity {
    private static final String TAG = "MainActivity";
    private NavController navController;
    private NavGraph navGraph;
    private boolean showOrderFragment = false;
    private int orderId = -1;
    @Inject
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_main);
        setupIntentData();
        setUpNavigation();
    }

    private void setupIntentData() {
        if (getIntent() != null && getIntent().getBooleanExtra("show_order", false)) {
            showOrderFragment = getIntent().getBooleanExtra("show_order", false);
            orderId = getIntent().getIntExtra(StringConstants.KEY_ORDER_ID, -1);
        }
    }

    private void setUpNavigation() {
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        navGraph = navController.getNavInflater().inflate(R.navigation.main_nav_graph);
        Bundle bundle = new Bundle();

        if (preferences.getBoolean(StringConstants.KEY_SHOULD_SHOW_MAP, true)) {
            navGraph.setStartDestination(R.id.map_fragment);
        } else if (getIntent() != null && getIntent().getBooleanExtra("show_order", false)) {
            bundle.putBoolean("show_order", showOrderFragment);
            bundle.putInt(StringConstants.KEY_ORDER_ID, orderId);
            navGraph.setStartDestination(R.id.order_status_fragment);
        } else {
            navGraph.setStartDestination(R.id.restaurant_fragment);
        }

        navController.setGraph(navGraph, bundle);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return super.onSupportNavigateUp();
    }
}
