package com.bydrone.droneapplication.Interface;

public interface CartAddButtonListener {
    void onItemCountChanged(int itemCount);
}
