package com.bydrone.droneapplication.Interface;

import com.bydrone.droneapplication.Models.FoodItem;

import org.json.JSONArray;

import java.util.ArrayList;

public interface OnItemUpdateListener {
    void onItemUpdate();
    void onMaxWeightReached();
}
