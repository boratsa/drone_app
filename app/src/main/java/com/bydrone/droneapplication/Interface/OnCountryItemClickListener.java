package com.bydrone.droneapplication.Interface;

public interface OnCountryItemClickListener {
    void onItemClick(String countryName,String countryCode);
}
