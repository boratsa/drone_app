package com.bydrone.droneapplication.Interface;

import com.bydrone.droneapplication.Models.Order;
import com.bydrone.droneapplication.Models.Restaurant;

public interface OnOrderItemClickListener {
    void onItemClick(Order order);
    void onInvoiceClick(Order order);
}
