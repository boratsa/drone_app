package com.bydrone.droneapplication.Interface;

import android.view.View;
import android.widget.ImageView;

import com.bydrone.droneapplication.Models.Restaurant;

public interface OnRestaurantClickListener {
    void onRestaurantClick(Restaurant restaurant, ImageView imageView);
}
