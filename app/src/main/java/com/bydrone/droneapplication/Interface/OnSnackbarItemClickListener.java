package com.bydrone.droneapplication.Interface;

/**
 * @author Ankur Khandelwal on 2019-07-15.
 */
public interface OnSnackbarItemClickListener {
    void onItemClick();
}
