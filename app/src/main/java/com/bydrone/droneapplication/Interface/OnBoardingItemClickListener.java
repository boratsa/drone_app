package com.bydrone.droneapplication.Interface;

/**
 * @author Ankur Khandelwal on 2019-07-12.
 */
public interface OnBoardingItemClickListener {
    void onClick(int position, String btn_type);
}
