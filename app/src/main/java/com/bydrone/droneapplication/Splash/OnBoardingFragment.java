package com.bydrone.droneapplication.Splash;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import com.bydrone.droneapplication.Adapter.OnBoardingAdapter;
import com.bydrone.droneapplication.Interface.OnBoardingItemClickListener;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.LogUtils;
import com.bydrone.droneapplication.Utils.SplashViewPager;
import com.bydrone.droneapplication.Utils.StringConstants;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class OnBoardingFragment extends Fragment implements OnBoardingItemClickListener {
    private String TAG = "OnBoardignFragment";
    private Activity activity;
    private Context context;
    private CountDownTimer countDownTimer;
    public static final int RequestPermissionCode = 7;
    private OnBoardingItemClickListener onWalkthroughItemClickListener;
    private boolean shouldAllowViewPagerScroll = false;
    private View rootView;

    @BindView(R.id.viewPager)
    SplashViewPager viewPager;
    @BindView(R.id.tabDots)
    TabLayout tabLayout;
    @BindView(R.id.coordinate_onboarding)
    CoordinatorLayout coordinateOnboarding;

    @Inject
    SharedPreferences preferences;

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        this.context = context;
        this.activity = getActivity();
        onWalkthroughItemClickListener = this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView  = inflater.inflate(R.layout.layout_onboarding,container,false);
        ButterKnife.bind(this,rootView);
        setupViewPager();
        return rootView;
    }

    
    private void setupViewPager() {
        tabLayout.setupWithViewPager(viewPager, true);
        OnBoardingAdapter onBoardingAdapter = new OnBoardingAdapter(context, onWalkthroughItemClickListener);
        viewPager.setAdapter(onBoardingAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                viewPager.setCurrentItem(i);
                if(i == 1 && !shouldAllowViewPagerScroll){
                    viewPager.disableScroll(true);
                }else if(shouldAllowViewPagerScroll){
                    viewPager.disableScroll(false);
                }else{
                    viewPager.disableScroll(false);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        LinearLayout tabLinearLayout = ((LinearLayout)tabLayout.getChildAt(0));
        for(int i = 0; i < tabLinearLayout.getChildCount(); i++) {
            tabLinearLayout.getChildAt(i).setOnTouchListener((v, event) -> true);
        }

    }

    private void checkForPermission() {
        if (checkPermissionIsEnabledOrNot()) {
            //all permission granted
            LogUtils.printLog(TAG, "All permission already granted");
            viewPager.setCurrentItem(2);
        } else {
            //Calling method to enable permission.
            requestMultiplePermission();
        }
    }

    private void requestMultiplePermission() {
        // Creating String Array with Permissions.
        requestPermissions(new String[]{ACCESS_FINE_LOCATION}, RequestPermissionCode);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                LogUtils.printLog(TAG, " --- permission granted result ---");
                if (grantResults.length > 0) {
                    boolean accessFineLocation = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (accessFineLocation) {
                        LogUtils.printLog(TAG, "Permission Granted");
                        viewPager.setCurrentItem(2);
                    } else {
                        LogUtils.printLog(TAG, "Permission Denied");
                        showSplashSnack(coordinateOnboarding, getResources().getString(R.string.dialog_location_text), 1);
                    }
                }
                break;
        }
    }

    private void showSplashSnack(CoordinatorLayout snackView, String string, int i) {
        Snackbar snackbar = Snackbar
                .make(snackView, string, Snackbar.LENGTH_INDEFINITE)
                .setActionTextColor(getResources().getColor(R.color.black))
                .setAction("Ok", view -> viewPager.setCurrentItem(i + 1));

        View view = snackbar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        view.setPadding(3,3,3,3);
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(getResources().getColor(R.color.white));
        snackbar.show();
    }

    public boolean checkPermissionIsEnabledOrNot() {
        int locationPermissionResult = ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION);
        return locationPermissionResult == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    public void onClick(int position, String btn_type) {
        if (btn_type.equals("never")) {
            showSplashSnack(coordinateOnboarding, getResources().getString(R.string.dialog_location_text), position);
        } else if (btn_type.equals("notification")) {
            if (position == 1) {
                checkForPermission();
            } else if (position == 2) {
                viewPager.setCurrentItem(position + 1);
            } else if (position == 3) {
                preferences.edit().putString(StringConstants.KEY_FIRST_TIME, "false").apply();
                startLoginActivity();
            }
        }
        shouldAllowViewPagerScroll = true;
    }

    private void startLoginActivity() {
        NavOptions options = new NavOptions.Builder().setPopUpTo(R.id.onboarding_fragment, true).build();
        Navigation.findNavController(rootView)
                .navigate(R.id.action_onboarding_to_loginFragment, null, options);
    }
}
