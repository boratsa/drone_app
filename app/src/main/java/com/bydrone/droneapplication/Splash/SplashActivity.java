package com.bydrone.droneapplication.Splash;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.bydrone.droneapplication.BaseActivity;
import com.bydrone.droneapplication.MainActivity;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.StringConstants;
import com.bydrone.droneapplication.Utils.Utility;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.NavGraph;
import androidx.navigation.Navigation;
import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;

public class SplashActivity extends BaseActivity {
    private String TAG = "SplashActivity";
    private NavController navController;
    private NavController mainNavController;
    private NavGraph navGraph;
    private boolean showOrderFragment = false;
    private CountDownTimer countDownTimer;

    @BindView(R.id.topLayout)
    RelativeLayout topLayout;
    @Inject
    SharedPreferences preferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash);
        ButterKnife.bind(this);
        startCountDown();
    }

    private void startCountDown() {
        countDownTimer = new CountDownTimer(10000, 500) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                topLayout.setVisibility(View.GONE);
                if (Utility.getJwtToken().equalsIgnoreCase("")) {
                    setUpAuthNavigation();
                }else{
                    startActivity(new Intent(SplashActivity.this,MainActivity.class));
                    SplashActivity.this.finish();
                }
            }
        }.start();
    }


    private void setUpAuthNavigation() {
        navController = Navigation.findNavController(this, R.id.splash_nav_host_fragment);
        navGraph = navController.getNavInflater().inflate(R.navigation.splash_nav_graph);
        Bundle bundle = new Bundle();

        if (preferences.getString(StringConstants.KEY_FIRST_TIME, "").equals("")) {
            navGraph.setStartDestination(R.id.onboarding_fragment);
        } else {
            navGraph.setStartDestination(R.id.login_fragment);
        }

        navController.setGraph(navGraph, bundle);
    }

    @Override
    public boolean onSupportNavigateUp() {
        return super.onSupportNavigateUp();
    }
}
