package com.bydrone.droneapplication.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bydrone.droneapplication.R;

public class CartAddButton extends LinearLayout {
    private TextView currentCount;
    private Button decreaseButton;
    private Button increaseButton;

    public interface cartAddButtonListener {
        void onItemCountChanged(int itemCount,int buttonType);
    }

    private int count = 0;
    private cartAddButtonListener listener;

    public void setItemChangedListener(cartAddButtonListener listener) {
        this.listener = listener;
    }

    public void setCount(int count) {
        this.count = count;
        if(count>=0) {
            currentCount.setText(String.valueOf(this.count));
        }
    }

    public CartAddButton(Context context, AttributeSet attrs){
        super(context, attrs);
        this.listener = null;

        // Inflate the custom widget layout xml file.
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        layoutInflater.inflate(R.layout.cart_add_button, this);

        currentCount = (TextView)findViewById(R.id.currentCount);
        increaseButton = (Button) findViewById(R.id.increase);
        decreaseButton = (Button)findViewById(R.id.decrease);
        setupUiColor(context,count);

        decreaseButton.setOnClickListener(view -> {
            if(count>0){
                count--;
            }

            if(count>=0 && count<=10) {
                currentCount.setText(String.valueOf(count));
                setupUiColor(context,count);
                if (listener != null)
                    listener.onItemCountChanged(count,0);
            }
        });

        increaseButton.setOnClickListener(view -> {
            if(count<10) {
                count++;
            }
            if(count>=0 && count<=10) {
                currentCount.setText(String.valueOf(count));
                setupUiColor(context,count);
                if (listener != null)
                    listener.onItemCountChanged(count,1);
            }
        });
    }

    private void setupUiColor(Context context,int count) {
//        if(count==0){
//            increaseButton.setBackground(context.getResources().getDrawable(R.drawable.button_circular));
//            increaseButton.setTextColor(context.getResources().getColor(R.color.black));
//
//            decreaseButton.setBackground(context.getResources().getDrawable(R.drawable.button_circular));
//            decreaseButton.setTextColor(context.getResources().getColor(R.color.black));
//        }else{
//            increaseButton.setBackground(context.getResources().getDrawable(R.drawable.button_circular_colored));
//            increaseButton.setTextColor(context.getResources().getColor(R.color.white));
//
//            decreaseButton.setBackground(context.getResources().getDrawable(R.drawable.button_circular_colored));
//            decreaseButton.setTextColor(context.getResources().getColor(R.color.white));
//        }
    }


}
