package com.bydrone.droneapplication.Utils;

import android.Manifest;

import com.google.android.gms.maps.model.LatLng;

/**
 * @author Ankur Khandelwal on 02/04/18.
 */

public class Constants {
//    public static final String BASE_URL = "http://ddrone-env-1.b5m9uhuzrg.eu-central-1.elasticbeanstalk.com";
    public static final String BASE_URL = "https://bydrone.ai";
    public static final String LOGIN = BASE_URL + "/api/account/auth/token/";
    public static final String ORDERS_HISTORY = BASE_URL + "/api/order/";
    public static final String ORDER_STATUS = BASE_URL + "/api/order/status/";
    public static final String RESTAURANTS_SEARCH = BASE_URL + "/api/restaurant/";
    public static final String FOOD_ITEMS = BASE_URL + "/api/restaurant/";
    public static final String CART = BASE_URL + "/api/restaurant/cart/";
    public static final String SERVICABLE_POINTS = BASE_URL + "/api/order/serviceable-points/";
    public static final String REGISTER = BASE_URL + "/api/account/signup/";
    public static final String PROFILE = BASE_URL + "/api/account/own-detail/";
    public static final String FORGOT_PASSWORD = BASE_URL + "/api/account/create-password-reset/";
    public static final String ORDER = BASE_URL +"/api/restaurant/order/";
    public static final String REGISTER_DEVICE_FCM = BASE_URL + "/api/notification/register/";
    public static final String GET_SERVER_PAYMENT_TOKEN = BASE_URL + "/api/payment/braintree/client_token/";
    public static final String SEND_PAYMENT_NONCE = "/api/payment_method_nonce/";
    public static final String GET_LIVE_DRONE_DATA = BASE_URL + "/api/drone/latest-data/";
    public static final String MODERATOR_DATA = BASE_URL + "/api/order/moderator/status/";
    public static final LatLng DEFAULT_LATLNG = new LatLng(60.189188, 24.831040);
    public static final String BYDRONE_URL  = "https://bydrone.ai/privacy_policy.html";
    public static final String BYDRONE_FEEDBACK_URL  = "https://bydrone.ai/feedback.html";


    //permissions strings
    public static final String READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;
    public static final String WRITE_EXTERNAL_STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public static final String CALL_PHONE_PERMISSION = Manifest.permission.CALL_PHONE;
}
