package com.bydrone.droneapplication.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatTextView;

/**
 * @author Ankur Khandelwal on 24/04/18.
 */


public class ByDroneTextView extends AppCompatTextView {

    private Typeface typeface;

    public ByDroneTextView(Context context) {
        super(context);
        setTypeFace();
    }

    public ByDroneTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setTypeFace();
    }

    public ByDroneTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeFace();
    }

    private void setTypeFace() {
        //add typeface
        typeface = Typeface.createFromAsset(getContext().getAssets(), "roboto-light.ttf");

        if (typeface == null) {
            typeface = Typeface.DEFAULT;
        }

        this.setTypeface(typeface);
    }
}
