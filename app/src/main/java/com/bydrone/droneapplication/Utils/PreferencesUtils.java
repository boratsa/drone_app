/**
 * Copyright (c) 2016 Affectiva Inc.
 * See the file license.txt for copying permission.
 */

package com.bydrone.droneapplication.Utils;

import android.content.SharedPreferences;


/**
 * A helper class to translate strings held in preferences into values to be used by the application.
 */
public class PreferencesUtils {

    static final int DEFAULT_FPS = 20;
    private final static String LOG_TAG = "EmotionTracker";

    /**
     * Attempt to parse and return FPS set by user. If the FPS is invalid, we set it to be the default FPS.
     */
    public static int getFrameProcessingRate(SharedPreferences pref) {
        String rateString = pref.getString("rate", String.valueOf(DEFAULT_FPS));
        int toReturn;
        try {
            toReturn = Integer.parseInt(rateString);
        } catch (Exception e) {
            saveFrameProcessingRate(pref, DEFAULT_FPS);
            return DEFAULT_FPS;
        }
        if (toReturn > 0) {
            return toReturn;
        } else {
            saveFrameProcessingRate(pref, DEFAULT_FPS);
            return DEFAULT_FPS;
        }
    }

    private static void saveFrameProcessingRate(SharedPreferences pref, int rate) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("rate", String.valueOf(rate));
        editor.commit();
    }

}
