package com.bydrone.droneapplication.Utils;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;

public class LocationHelper {
    public Context context;

    public void init(Context context){
        this.context= context;
    }

    private Address getAddressFromLatLng(double latitude, double longitude) {
        Geocoder geocoder;
        List<Address> addresses = new ArrayList<>();
        geocoder = new Geocoder(context, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if(addresses!=null && addresses.size()>0) {
                return addresses.get(0);
            }else{
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;

    }

    public String getAddress(Location location) {
        LogUtils.printLog(TAG,"location in helper= "+location);
        if (location!=null && Double.valueOf(location.getLatitude()) != null || Double.valueOf(location.getLongitude()) != null) {
            Address start_locationAddress = getAddressFromLatLng(location.getLatitude(), location.getLongitude());
            if (start_locationAddress != null) {
                String address = start_locationAddress.getAddressLine(0);
                LogUtils.printLog(TAG, "currentStartLocation = " + address);
                return address;
            } else {
                return "";
            }
        }
        return "";
    }



    public LatLng getLocationFromAddress(String strAddress) {
        Geocoder coder = new Geocoder(context);
        List<Address> address;
        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address == null || address.size() ==0) {
                return null;
            }
            Log.i(TAG, "GEOCODED ADDRESS: "+address);
            LatLng latLng = null;
            for(int i=0;i<address.size();i++) {
                Address location = address.get(i);
                latLng = new LatLng(location.getLatitude(), location.getLongitude());
            }
            return latLng;
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public double calculateBusStopDistanceFromUser(double lat1, double lon1, double lat2, double lon2) {
        Location locationA = new Location("origin");
        locationA.setLatitude(lat1);
        locationA.setLongitude(lon1);

        Location locationB = new Location("dest");
        locationB.setLatitude(lat2);
        locationB.setLongitude(lon2);
        return locationA.distanceTo(locationB);
    }

    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


}
