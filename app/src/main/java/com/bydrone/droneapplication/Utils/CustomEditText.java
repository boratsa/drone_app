package com.bydrone.droneapplication.Utils;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;

/**
 * @author Ankur Khandelwal on 2019-07-17.
 */
public class CustomEditText extends AppCompatEditText {
    float leftPadding = -1;

    public CustomEditText(Context context) {
        super(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        addPrefix();
    }

    private void addPrefix() {
        if (leftPadding == -1) {
            String prefix = (String) getTag();
            float[] widths = new float[prefix.length()];
            getPaint().getTextWidths(prefix, widths);
            float textWidth = 0;
            for (float w : widths) {
                textWidth += w;
            }
            leftPadding = getCompoundPaddingLeft();
            setPadding((int) (textWidth + leftPadding), getPaddingRight(), getPaddingTop(), getPaddingBottom());
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        String prefix = (String) getTag();
        canvas.drawText(prefix, leftPadding, getLineBounds(0,null), getPaint());
    }
}
