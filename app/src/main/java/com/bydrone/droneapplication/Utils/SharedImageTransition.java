package com.bydrone.droneapplication.Utils;

import androidx.transition.ChangeBounds;
import androidx.transition.ChangeImageTransform;
import androidx.transition.ChangeTransform;
import androidx.transition.Fade;
import androidx.transition.TransitionSet;

/**
 * @author Ankur Khandelwal on 2019-08-24.
 */
public class SharedImageTransition extends TransitionSet {
    public SharedImageTransition(){
        setOrdering(ORDERING_TOGETHER);
        addTransition(new ChangeBounds()).
                addTransition(new ChangeTransform()).
                addTransition(new ChangeImageTransform());

    }
}
