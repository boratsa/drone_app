package com.bydrone.droneapplication.Utils;

import android.util.Log;

import com.bydrone.droneapplication.BuildConfig;

/**
 * @author Ankur Khandelwal on 30/05/17.
 */

public class LogUtils {
    public static void printLog(String tag, String message) {
        if (BuildConfig.DEBUG_MODE) {
            Log.d(tag, message);
        }
    }
}
