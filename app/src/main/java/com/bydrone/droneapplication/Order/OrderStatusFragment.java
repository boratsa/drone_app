package com.bydrone.droneapplication.Order;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.Models.FoodItem;
import com.bydrone.droneapplication.Models.Order;
import com.bydrone.droneapplication.Models.Status;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.Constants;
import com.bydrone.droneapplication.Utils.LogUtils;
import com.bydrone.droneapplication.Utils.StringConstants;
import com.bydrone.droneapplication.Utils.Utility;
import com.bydrone.droneapplication.Utils.WrappedLinearLayoutManager;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderStatusFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = "OrderStatusFragment";
    private List<Status> statusList;
    private int pendingItemIndex = 0;
    private int orderID = -1;
    private Timer timer = null;
    private Timer liveTimer = null;
    private WrappedLinearLayoutManager linearLayoutManager;
    private GoogleMap mMap;
    private MarkerOptions markerOptions;
    private Order selectedOrder;
    private Location sourceLocation = new Location("");
    private Location destLocation = new Location("");
    private ArrayList<Location> locations = new ArrayList<>();
    private int scrollPosition = 0;
    private boolean isSwiped = false;
    private Marker droneMarker;
    private ArrayList<View> orderViewsList = new ArrayList<>();
    private View rootView;
    private Context context;
    private AppCompatActivity activity;
    private BottomSheetBehavior bottomSheetBehavior;
    @BindView(R.id.swipeButton)
    ExtendedFloatingActionButton swipeButton;
    @BindView(R.id.warning_btn)
    Button warning_btn;
    @BindView(R.id.toolbar_order_status)
    Toolbar toolbarOrderStatus;
    @BindView(R.id.orderView1)
    LinearLayout orderView1;
    @BindView(R.id.orderView2)
    LinearLayout orderView2;
    @BindView(R.id.orderView3)
    LinearLayout orderView3;
    @BindView(R.id.orderView4)
    LinearLayout orderView4;
    @BindView(R.id.orderView5)
    LinearLayout orderView5;
    @BindView(R.id.orderView6)
    LinearLayout orderView6;
    @BindView(R.id.orderView7)
    LinearLayout orderView7;
    @BindView(R.id.orderView8)
    LinearLayout orderView8;
    @BindView(R.id.orderView9)
    LinearLayout orderView9;
    @BindView(R.id.orderViews)
    LinearLayout orderViews;
    @BindView(R.id.scrollView)
    HorizontalScrollView scrollView;
    @BindView(R.id.eta_fab)
    ExtendedFloatingActionButton etaFabIcon;
    @BindView(R.id.eta_invoice)
    ExtendedFloatingActionButton etaInvoice;
    @BindView(R.id.navigate)
    ExtendedFloatingActionButton navigateFabBtn;
    @BindView(R.id.bottom_sheet_order_details)
    RelativeLayout bottomSheetOrderDetails;
    @BindView(R.id.text_items)
    TextView textItems;
    @BindView(R.id.items_description)
    TextView itemsDescription;
    @BindView(R.id.restaurant_name)
    TextView restaurantName;
    @BindView(R.id.restaurant_location)
    TextView restaurantLocation;
    @BindView(R.id.delivery_location)
    TextView deliveryLocation;
    @BindView(R.id.date_text)
    TextView dateText;
    @BindView(R.id.date)
    TextView dateTextView;
    @BindView(R.id.total_amount)
    TextView totalAmount;
    @BindView(R.id.instruction_header)
    RelativeLayout instructionHeader;
    @BindView(R.id.toggle_arrow)
    ImageView toggleArrowImage;
    @BindView(R.id.order_cancel_view)
    TextView orderCancelView;
    @Inject
    public SharedPreferences preferences;

    @Inject
    public ApiClientInterface apiClientInterface;

    @Inject
    public CartSingleton cartSingleton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_order_status,container, false);
        ButterKnife.bind(this,rootView);
        setupUI();
        return rootView;
    }

    /**
     * First method to call after activity onCreate()
     * @param context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    public void setupUI(){
        activity.setSupportActionBar(toolbarOrderStatus);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarOrderStatus.setNavigationOnClickListener(v -> Navigation.findNavController(rootView).navigateUp());

        toolbarOrderStatus.setTitle(getResources().getString(R.string.order_status_title));
        toolbarOrderStatus.setTitleTextColor(getResources().getColor(R.color.black));
        toolbarOrderStatus.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black));

        if (getArguments() != null) {
            orderID = getArguments().getInt(StringConstants.KEY_ORDER_ID,-1);
        }

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetOrderDetails);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        createDefaultViews();
        statusList = new ArrayList<>();
        linearLayoutManager = new WrappedLinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        fetchOrderDetails(orderID);
    }

    private void createDefaultViews() {
        orderViewsList.add(orderView1);
        orderViewsList.add(orderView2);
        orderViewsList.add(orderView3);
        orderViewsList.add(orderView4);
        orderViewsList.add(orderView5);
        orderViewsList.add(orderView6);
        orderViewsList.add(orderView7);
        orderViewsList.add(orderView8);
        orderViewsList.add(orderView9);
    }

    private void fetchOrderDetails(int orderID) {
        try {
            Single<Order> orderObservable = apiClientInterface.getOrderDetail("Bearer " + Utility.getJwtToken(), String.valueOf(orderID));
            orderObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(getOrderObserver());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SingleObserver<? super Order> getOrderObserver() {
        return new SingleObserver<Order>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(Order order) {
                selectedOrder = order;
                setupUi();
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(context.getApplicationContext(), getResources().getString(R.string.error_generic), Toast.LENGTH_LONG).show();
            }
        };
    }

    private void setupUi() {
        setupBottomSheet();
        setLocations();
        setMapFragment();
        checkOrderStatus();
        getLiveDroneData();
    }

    @OnClick({R.id.instruction_header})
    void toggleBottomSheet(){
        if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            toggleArrowImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_arrow_up));
        }else if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            toggleArrowImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_arrow_down));
        }
    }

    private void setupBottomSheet() {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        float pixels = Utility.convertDpToPixel(context,40);
        bottomSheetBehavior.setPeekHeight((int)pixels,true);
        if(selectedOrder!=null) {
            restaurantName.setTypeface(null, Typeface.BOLD);
            itemsDescription.setTypeface(null, Typeface.BOLD);
            deliveryLocation.setTypeface(null, Typeface.BOLD);
            dateTextView.setTypeface(null, Typeface.BOLD);
            totalAmount.setTypeface(null, Typeface.BOLD);
            String itemText = "";
            restaurantName.setText(selectedOrder.getSourceLocation().getName());
            for (FoodItem item : selectedOrder.getGoods().getItems()) {
                itemText += item.getQuantity() + " x " + item.getName() + " , ";
            }
            if (itemText.length() > 0) {
                itemText = itemText.substring(0, itemText.length() - 2);
            }
            textItems.setText(itemText);

            Date date = Utility.getDateFromString(selectedOrder.getDate());
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            String calendarDay = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
            int calendarMonth = calendar.get(Calendar.MONTH);
            String calendarYear = String.valueOf(calendar.get(Calendar.YEAR));
            String calendarHour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
            String calendarMin = String.valueOf(calendar.get(Calendar.MINUTE));
            if (calendar.get(Calendar.MINUTE) < 10) {
                calendarMin = "0" + calendarMin;
            }
            String calendarMonthName = Utility.getMonthName(calendarMonth);
            if (date != null) {
                String dateStr = calendarDay + "-" + calendarMonthName + "-" + calendarYear + "  " + calendarHour + ":" + calendarMin + ":00 EEST";
                dateTextView.setText(dateStr);
            }
            restaurantLocation.setText(selectedOrder.getSourceLocation().getAddress());
            deliveryLocation.setText(selectedOrder.getDestinationLocation().getAddress());
            itemsDescription.setText(itemText);
            String amountStr = selectedOrder.getAmount() + context.getResources().getString(R.string.euro);
            totalAmount.setText(amountStr);
        }
    }

    private void getLiveDroneData() {
        try {
            Call<ResponseBody> liveDataResponse = apiClientInterface.getLiveDroneData("Bearer " + Utility.getJwtToken());
            liveDataResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    LogUtils.printLog(TAG, "code =" + response.code());
                    try {
                        if (response.code() == 200 || response.code() == 201) {
                            if (response.body() != null) {
                                JSONObject droneObj = new JSONObject(response.body().string());
                                LogUtils.printLog(TAG, "response " + response.body());
                                double lat = droneObj.getJSONObject("data").getJSONObject("location").getDouble("lat");
                                double lng = droneObj.getJSONObject("data").getJSONObject("location").getDouble("lng");
                                setupLiveDroneUi(lat, lng);

                                if (liveTimer != null) {
                                    liveTimer.cancel();
                                    liveTimer = null;
                                }
                                liveTimer = new Timer();
                                liveTimer.schedule(new java.util.TimerTask() {
                                                       @Override
                                                       public void run() {
                                                           getLiveDroneData();
                                                       }
                                                   },
                                        10000
                                );
                            }
                        } else {

                            if (response.errorBody() != null) {
                                JSONObject object = new JSONObject(response.errorBody().string());
                                String message = object.getString("error");
                                Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    //TODO remove this line from failure
                    //progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupLiveDroneUi(Double lat, Double lng) {
        if (droneMarker != null)
            droneMarker.remove();
        try {
            droneMarker = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lng))
                    .anchor(0.5f, 0.5f)
                    .title("ByDrone")
                    .icon(Utility.bitmapDescriptorFromVector(context, R.drawable.ic_drone_green)));
        }catch (Exception e){
            LogUtils.printLog(TAG,"error in play store service");
//            Toast.makeText(context,context.getResources().getString(R.string.play_service_error),Toast.LENGTH_LONG).show();
        }
    }

    private void setLocations() {
        if (selectedOrder != null) {
            sourceLocation.setLatitude(60.184730);
            sourceLocation.setLongitude(24.825144);

            destLocation.setLatitude(selectedOrder.getDestinationLocation().getCoordinates().getLat());
            destLocation.setLongitude(selectedOrder.getDestinationLocation().getCoordinates().getLng());

            locations.add(sourceLocation);
            locations.add(destLocation);
        }

    }

    private void setMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.status_map);
        if (mapFragment != null)
            mapFragment.getMapAsync(this);

        MapsInitializer.initialize(context);
    }


    @OnClick(R.id.navigate)
    public void startGoogleMapForNavigation(){
        double destinationLatitude = selectedOrder.getDestinationLocation().getCoordinates().getLat();
        double destinationLongitude = selectedOrder.getDestinationLocation().getCoordinates().getLng();
        String destinationAdd =  selectedOrder.getDestinationLocation().getName();
        String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?daddr=%f,%f (%s)", destinationLatitude, destinationLongitude, destinationAdd);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }


    private void checkOrderStatus() {
        try {
            final boolean[] isCancelled = {false};
            Call<ResponseBody> orderStatusResponse = apiClientInterface.orderStatus("Bearer " + Utility.getJwtToken(), String.valueOf(orderID));
            orderStatusResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    //progressBar.setVisibility(View.GONE);
                    //statusList = new ArrayList<>();
                    LogUtils.printLog(TAG, "code =" + response.code());
                    try {
                        if (response.code() == 200 || response.code() == 201) {
                            if (response.body() != null) {
                                JSONObject object = new JSONObject(response.body().string());
                                LogUtils.printLog(TAG, "RESPONSEDATA --->  =" + object.toString());
                                //object.getString("access_token");

                                String etaStr = object.getString("eta");
                                setETA(etaStr);
                                JSONArray array = object.getJSONArray("events");

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jo = array.getJSONObject(i);
                                    int statusId = jo.getInt("status");
                                    Status contacts = new Status(statusId, jo.getString("status_text"), jo.getString("time"), false);
                                    statusList.add(contacts);
                                    if(statusId == -1){
                                        isCancelled[0] = true;
                                    }
                                }
//                                scrollPosition = array.length() - 1;
                                JSONArray pendingItems = object.getJSONArray("pending");
                                if (pendingItems.length() > 0) {
                                    pendingItemIndex = Integer.parseInt((pendingItems.getJSONObject(0).getString("status")));
                                }

                                for (int i = 0; i < pendingItems.length(); i++) {
                                    JSONObject jo = pendingItems.getJSONObject(i);
                                    int statusId = jo.getInt("status");
                                    Status contacts = new Status(statusId, jo.getString("status_text"), null, true);
                                    statusList.add(contacts);
                                    if(statusId == -1){
                                        isCancelled[0] = true;
                                    }
                                }

                                setupRecyclerView(statusList, isCancelled[0]);
                                if (timer != null) {
                                    timer.cancel();
                                    timer = null;
                                }
                                timer = new Timer();
                                timer.schedule(new java.util.TimerTask() {
                                                   @Override
                                                   public void run() {
                                                       statusList.clear();
                                                       checkOrderStatus();
                                                   }
                                               },
                                        10000
                                );
                            }
                        } else {

                            if (response.errorBody() != null) {
                                JSONObject object = new JSONObject(response.errorBody().string());
                                String message = object.getString("error");
                                Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    //TODO remove this line from failure
                    //progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setETA(String dateStr) {
        Date date_cur = new Date();
        Date etaDate = Utility.getDateFromString(dateStr);

        long eta_diff = date_cur.getTime() - etaDate.getTime();
        long diffInMin_eta = Math.abs(TimeUnit.MILLISECONDS.toMinutes(eta_diff));

        etaFabIcon.setText("ETA: " + diffInMin_eta + " mins");
    }

    private void setupRecyclerView(List<Status> contacts, boolean isOrderCancelled) {
        orderViews.removeAllViews();
        scrollView.removeAllViews();

        TextView nameTextView;
        TextView subTitleTextView;
        TextView statusDescription;
        FrameLayout mItemLine;
        CheckBox approvalCheckbox;
        View statusIconView;
        LogUtils.printLog(TAG,"contactsList = "+contacts.toString() + " & isOrdercancelled = " +isOrderCancelled) ;
        if(!isOrderCancelled) {
            for (int position = 0; position < contacts.size(); position++) {
                Status contact = contacts.get(position);
                View itemView = orderViewsList.get(position);
                nameTextView = itemView.findViewById(R.id.item_title);
                subTitleTextView = itemView.findViewById(R.id.item_subtitle);
                statusDescription = itemView.findViewById(R.id.status_description);
                mItemLine = itemView.findViewById(R.id.item_line);
                approvalCheckbox = itemView.findViewById(R.id.approvalBox);
                statusIconView = itemView.findViewById(R.id.statusIcon);
                nameTextView.setVisibility(View.VISIBLE);
                subTitleTextView.setVisibility(View.VISIBLE);
                mItemLine.setVisibility(View.VISIBLE);

                nameTextView.setText(contact.getStatus_text());
                if (!contact.getIsPending()) {
                    String descriptionText = cartSingleton.getStatusDescriptionText(contact.getStatus());
                    statusDescription.setText(descriptionText);
                    statusIconView.setBackground(getResources().getDrawable(R.drawable.ic_correct_fill));
                } else {
                    statusIconView.setBackground(getResources().getDrawable(R.drawable.ic_arrow_down));
                }


                if (contact.getStatus() == -1) {
                    triggerCancelledView();
                }


                if (pendingItemIndex == 7 && !isSwiped) {
                    swipeButton.setVisibility(View.VISIBLE);
                    approvalCheckbox.setVisibility(View.GONE);
                    approvalCheckbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                        updateStatus(pendingItemIndex);

                    });
                }

                if (position == 0) {
                    mItemLine.setBackground(getResources().getDrawable(R.drawable.line_bg_top));
                } else if (position == 8) {
                    mItemLine.setBackground(getResources().getDrawable(R.drawable.line_bg_bottom));
                } else {
                    mItemLine.setBackground(getResources().getDrawable(R.drawable.line_bg_middle));
                }

                orderViews.addView(itemView);
            }
            scrollView.addView(orderViews);
            if (pendingItemIndex == 8) {
                etaFabIcon.setVisibility(View.GONE);
            }
            if(pendingItemIndex>0) {
                scrollView.smoothScrollTo(orderViewsList.get(pendingItemIndex - 1).getLeft(), orderViewsList.get(pendingItemIndex - 1).getTop());
            }
        }else{
            scrollView.setVisibility(View.GONE);
            orderCancelView.setVisibility(View.VISIBLE);
            navigateFabBtn.setVisibility(View.GONE);
            etaFabIcon.setVisibility(View.GONE);
            etaInvoice.setVisibility(View.VISIBLE);
        }

    }

    private void triggerCancelledView() {
        scrollView.setVisibility(View.GONE);
        orderCancelView.setVisibility(View.VISIBLE);
        timer.cancel();
    }

    private void updateStatus(int statusID) {
        try {
            Call<ResponseBody> updateStatusResponse = apiClientInterface.orderUpdateStatus("Bearer " + Utility.getJwtToken(), String.valueOf(orderID), statusID);
            updateStatusResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    //progressBar.setVisibility(View.GONE);
                    LogUtils.printLog(TAG, "code =" + response.code());
                    try {
                        if (response.code() == 200 || response.code() == 201) {
                            if (response.body() != null) {
                                JSONObject object = new JSONObject(response.body().string());
                                LogUtils.printLog(TAG, "RESPONSEDATA --->  =" + object.toString());
                                //object.getString("access_token");
                                if (timer != null) {
                                    timer.cancel();
                                }
                            }
                        } else {

                            if (response.errorBody() != null) {
                                JSONObject object = new JSONObject(response.errorBody().string());
                                String message = object.getString("error");
                                Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    //TODO remove this line from failure
                    //progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LogUtils.printLog(TAG, "map ready");
        mMap = googleMap;
//        setCameraOnSpecificLatLng();
        setupLocationMarkerOnUi();
        mMap.setOnMarkerClickListener(marker -> {
            LogUtils.printLog(TAG, "marker clicked");
            return false;
        });
//        mMap.getUiSettings().setScrollGesturesEnabled(false);
    }

    private void setCameraOnSpecificLatLng() {
        if (mMap != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Constants.DEFAULT_LATLNG, 8));
        }
    }

    private void setupLocationMarkerOnUi() {
        LogUtils.printLog(TAG, "sourceLoc = " + sourceLocation.getLatitude() + " , " + sourceLocation.getLongitude());
        LogUtils.printLog(TAG, "destLoc = " + destLocation.getLatitude() + " , " + destLocation.getLongitude());
        Marker m1 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(sourceLocation.getLatitude(), sourceLocation.getLongitude()))
                .anchor(0.5f, 0.5f)
                .icon(Utility.bitmapDescriptorFromVector(context, R.drawable.ic_ellipse)));

        if(selectedOrder!=null){
            m1.setTitle(selectedOrder.getSourceLocation().getName());
        }

        Marker m2 = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(destLocation.getLatitude(), destLocation.getLongitude()))
                .anchor(0.5f, 0.5f)
                .title("ByDrone Landing Pad")
                .icon(bitmapDescriptorFromVector(context)));

//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        builder.include(m1.getPosition());
//        builder.include(m2.getPosition());
//        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 15));

        drawPolyLine(sourceLocation, destLocation);
        LatLng latLng = new LatLng(sourceLocation.getLatitude(), sourceLocation.getLongitude());
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));
    }

    private void drawPolyLine(Location sourceLocation, Location destLocation) {
        PolylineOptions line = new PolylineOptions().add(
                new LatLng(sourceLocation.getLatitude(), sourceLocation.getLongitude()),
                new LatLng(destLocation.getLatitude(), destLocation.getLongitude()))
                .width(5).color(getResources().getColor(R.color.polyline));

        mMap.addPolyline(line);
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_polygon);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());

        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }


    @OnClick(R.id.swipeButton)
    public void swipeButtonClick() {
        swipeButton.setVisibility(View.GONE);
        warning_btn.setVisibility(View.VISIBLE);
        isSwiped = true;
        updateStatus(7);
    }

    @OnClick(R.id.eta_invoice)
    public void downloadInvoice(){
        if(selectedOrder.getInvoiceUrl()!=null) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(selectedOrder.getInvoiceUrl()));
            startActivity(browserIntent);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (timer != null)
            timer.cancel();

        if (liveTimer != null)
            liveTimer.cancel();
    }


}//End of top level classs
