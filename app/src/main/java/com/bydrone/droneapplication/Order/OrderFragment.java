package com.bydrone.droneapplication.Order;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bydrone.droneapplication.Adapter.OrdersAdapter;
import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.Interface.OnOrderItemClickListener;
import com.bydrone.droneapplication.Models.Order;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.StringConstants;
import com.bydrone.droneapplication.Utils.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OrderFragment extends Fragment implements OnOrderItemClickListener {
    private static final String TAG = "OrderFragment";
    private ArrayList<Order> ordersList;
    private OrdersAdapter adapter;
    private View rootView;
    private Context context;
    private AppCompatActivity activity;

    @BindView(R.id.toolbar_order)
    Toolbar toolbarOrder;
    @BindView(R.id.orderRecyclerView)
    RecyclerView orderRecyclerView;
    @BindView(R.id.no_order_layout)
    RelativeLayout noOrderLayout;

    OnOrderItemClickListener onOrderItemClickListener;


    @Inject
    public SharedPreferences preferences;

    @Inject
    public ApiClientInterface apiClientInterface;

    @Inject
    public CartSingleton cartSingleton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_orders,container, false);
        ButterKnife.bind(this,rootView);
        setupUI();
        return rootView;
    }

    /**
     * First method to call after activity onCreate()
     * @param context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }


    private void setupUI() {
        activity.setSupportActionBar(toolbarOrder);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbarOrder.setNavigationOnClickListener(v -> Navigation.findNavController(rootView).navigateUp());

        toolbarOrder.setTitle(getResources().getString(R.string.order_history));
        toolbarOrder.setTitleTextColor(getResources().getColor(R.color.black));
        toolbarOrder.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black));
        onOrderItemClickListener = this;
        ordersList = new ArrayList<>();
        fetchAllOrders();
    }

    private void fetchAllOrders() {
        Single<List<Order>> orderObservable = apiClientInterface.getOrders("Bearer " + Utility.getJwtToken());
        orderObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getOrderObserver());
    }

    private SingleObserver<? super List<Order>> getOrderObserver() {
        return new SingleObserver<List<Order>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onSuccess(List<Order> orders) {
                ordersList.addAll(orders);
                if (ordersList.size() > 0) {
                    Collections.reverse(ordersList);
                    adapter = new OrdersAdapter(ordersList, context, onOrderItemClickListener,cartSingleton);
                    orderRecyclerView.setAdapter(adapter);
                    orderRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                    toggleUI(true);
                } else {
                    toggleUI(false);
                }
            }

            @Override
            public void onError(Throwable e) {
                toggleUI(false);
            }
        };
    }

    private void toggleUI(Boolean shouldShowOrder) {
        if (shouldShowOrder) {
            orderRecyclerView.setVisibility(View.VISIBLE);
            noOrderLayout.setVisibility(View.GONE);
        } else {
            orderRecyclerView.setVisibility(View.GONE);
            noOrderLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onItemClick(Order order) {
        cartSingleton.setSelectedOrder(order);
        Bundle bundle = new Bundle();
        bundle.putInt(StringConstants.KEY_ORDER_ID,order.getId());
        Navigation.findNavController(rootView)
                .navigate(R.id.action_order_to_order_status_fragment,bundle,null,null);
    }

    @Override
    public void onInvoiceClick(Order order) {
        if(order.getInvoiceUrl()!=null) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(order.getInvoiceUrl()));
            startActivity(browserIntent);
        }
    }
}
