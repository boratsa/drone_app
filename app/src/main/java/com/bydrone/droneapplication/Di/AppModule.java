package com.bydrone.droneapplication.Di;


import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.bydrone.droneapplication.DroneApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private final DroneApplication droneApplication;

    public AppModule(DroneApplication application) {
        this.droneApplication = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return droneApplication;
    }

    @Provides
    @Singleton
    SharedPreferences provideSharedPreference(Context context) {
        return context.getSharedPreferences("prefs", Context.MODE_PRIVATE);
    }
}
