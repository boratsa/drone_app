package com.bydrone.droneapplication.Di;

import com.bydrone.droneapplication.Cart.CartFragment;
import com.bydrone.droneapplication.Fragment.FinishFragment;
import com.bydrone.droneapplication.Fragment.FoodItemFragment;
import com.bydrone.droneapplication.Fragment.HelpFragment;
import com.bydrone.droneapplication.Login.LoginFragment;
import com.bydrone.droneapplication.Login.SignUpFragment;
import com.bydrone.droneapplication.Maps.MapsFragment;
import com.bydrone.droneapplication.Order.OrderFragment;
import com.bydrone.droneapplication.Order.OrderStatusFragment;
import com.bydrone.droneapplication.Fragment.ProfileFragment;
import com.bydrone.droneapplication.Fragment.RestaurantFragment;
import com.bydrone.droneapplication.Splash.OnBoardingFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentBuilder {
    @ContributesAndroidInjector
    abstract RestaurantFragment bindRestaurantFragment();
    @ContributesAndroidInjector
    abstract FoodItemFragment bindFoodItemFragment();
    @ContributesAndroidInjector
    abstract CartFragment bindCartFragment();
    @ContributesAndroidInjector
    abstract FinishFragment bindFinishFragment();
    @ContributesAndroidInjector
    abstract OrderStatusFragment bindOrderStatusFragment();
    @ContributesAndroidInjector
    abstract OrderFragment bindOrderFragment();
    @ContributesAndroidInjector
    abstract ProfileFragment bindProfileFragment();
    @ContributesAndroidInjector
    abstract MapsFragment bindMapFragment();
    @ContributesAndroidInjector
    abstract HelpFragment bindHelpFragment();

    @ContributesAndroidInjector
    abstract LoginFragment bindLoginFragment();
    @ContributesAndroidInjector
    abstract SignUpFragment bindSignupFragment();
    @ContributesAndroidInjector
    abstract OnBoardingFragment bindOnBoardingFragment();

}
