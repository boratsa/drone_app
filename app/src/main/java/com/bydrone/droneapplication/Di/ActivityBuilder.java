package com.bydrone.droneapplication.Di;

import com.bydrone.droneapplication.BaseActivity;
import com.bydrone.droneapplication.MainActivity;
import com.bydrone.droneapplication.Splash.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {
    @ContributesAndroidInjector
    abstract BaseActivity bindBaseActivity();
    @ContributesAndroidInjector
    abstract SplashActivity bindSplashActivity();
    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();
}
