package com.bydrone.droneapplication.Di;

import com.bydrone.droneapplication.BaseActivity;
import com.bydrone.droneapplication.Cart.CartModule;
import com.bydrone.droneapplication.DroneApplication;
import com.bydrone.droneapplication.Networking.ApiModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
@Component(modules = {
		AppModule.class,
		ApiModule.class,
		CartModule.class,
		ActivityBuilder.class,
		FragmentBuilder.class,
		AndroidSupportInjectionModule.class})

public interface AppComponent {
	@Component.Builder
	interface Builder{
		@BindsInstance Builder application(DroneApplication application);
		Builder appModule(AppModule appModule);
		AppComponent build();
	}

	void inject(DroneApplication application);
}
