package com.bydrone.droneapplication;
import android.app.Activity;
import android.app.Application;

import com.bydrone.droneapplication.Di.AppComponent;
import com.bydrone.droneapplication.Di.AppModule;
import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.Di.DaggerAppComponent;
import com.bydrone.droneapplication.Utils.PreferencesManager;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;

import androidx.fragment.app.Fragment;
import androidx.multidex.MultiDex;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasFragmentInjector;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * @author Ankur Khandelwal on 27/03/18.
 */

public class DroneApplication extends Application implements HasActivityInjector, HasSupportFragmentInjector {
    AppComponent appComponent;
    @Inject DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;
    @Inject DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().application(this).appModule(new AppModule(this)).build();
        appComponent.inject(this);
        setupLibIntialization();
    }

    private void setupLibIntialization() {
        PreferencesManager.initialize(this);
        MultiDex.install(this);
        FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
    }


    @Override
    public AndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }
}
