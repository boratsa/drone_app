package com.bydrone.droneapplication.Maps;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.transition.Transition;
import androidx.transition.TransitionInflater;

import com.bydrone.droneapplication.BaseActivity;
import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.MainActivity;
import com.bydrone.droneapplication.Models.LandingLocation;
import com.bydrone.droneapplication.Networking.APIClient;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.Constants;
import com.bydrone.droneapplication.Utils.LocationHelper;
import com.bydrone.droneapplication.Utils.LogUtils;
import com.bydrone.droneapplication.Utils.MarshMallowPermission;
import com.bydrone.droneapplication.Utils.StringConstants;
import com.bydrone.droneapplication.Utils.Utility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveStartedListener {
    private static final String TAG = "MapsFragment";

    //location variables
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final int REQUEST_CHECK_SETTINGS = 0x1;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 20 * 1000;
    private final int REQ_CODE_SPEECH_INPUT = 100;
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;
    private final static String KEY_REQUESTING_LOCATION_UPDATES = "requesting-location-updates";
    private final static String KEY_LOCATION = "location";
    private final static String KEY_LAST_UPDATED_TIME_STRING = "last-updated-time-string";
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private Boolean mRequestingLocationUpdates=false;
    private String mLastUpdateTime;
    private Marker mCurrLocationMarker;
    private MarkerOptions markerOptions;
    private GoogleMap mMap;
    private LatLng userLatLng = new LatLng((long) 0.0, (long) 0.0);
    private Boolean zoomRequire;
    private Context context;
    private LandingLocation selectedLandingLocation = null;
    private BottomSheetBehavior bottomSheetBehavior;
    private Boolean isGestureRecorded = false;
    private Boolean shouldFetchLandingLocation = false;
    private AppCompatActivity activity;
    private boolean requestingCurrentLocation = false;
    private LocationHelper helper;

    private MarshMallowPermission marshMallowPermission;
    private String sourceAddress = null;
    private GeofencingClient geofencingClient;
    private ArrayList<LandingLocation> landingLocations;
    private ArrayList<Marker> landingMarkers;
    private boolean firstTime = true;
    private View rootView;
    private GoogleApiAvailability googleApiAvailability;
    @BindView(R.id.bottom_location_view)
    RelativeLayout bottom_location_view;
    @BindView(R.id.progress_layout)
    RelativeLayout progress_layout;
    @BindView(R.id.fab_current_location)
    FloatingActionButton fab_current_location;
    @BindView(R.id.current_location)
    EditText current_location;
    @BindView(R.id.confirm_pickup_btn)
    Button confirm_pickup_btn;
    @BindView(R.id.fab_map_type)
    FloatingActionButton fab_map_type;
    @BindView(R.id.location_img_up)
    ImageView location_img_up;
    @BindView(R.id.cancel_button)
    ImageView cancelButton;
    @BindView(R.id.progressBar)
    ProgressBar location_progress;
    @BindView(R.id.bottom_sheet)
    LinearLayout layoutBottomSheet;
    @BindView(R.id.outside_btn)
    Button outside_btn;
    @BindView(R.id.progress_text)
    TextView progress_text_view;
    @BindView(R.id.coordinate_map)
    CoordinatorLayout coordinate_map;
    @BindView(R.id.info_layout)
    RelativeLayout infoLayout;
    @BindView(R.id.location_select_layout)
    RelativeLayout locationSelectLayout;
    Button confirm_button;
    @BindView(R.id.select_location_btn)
    TextView selectLocationBtn;
    @BindView(R.id.select_location_text)
    TextView selectLocationText;
    @BindView(R.id.explore_delivey_area)
    Button exploreDeliveryArea;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.toolbar_map)
    Toolbar toolbarMap;
    @BindView(R.id.progressBar_map)
    ProgressBar progressBarMap;

    @Inject
    public SharedPreferences preferences;

    @Inject
    public ApiClientInterface apiClientInterface;

    @Inject
    public CartSingleton cartSingleton;
    
    /**
     * First method to call after activity onCreate()
     * @param context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    /**
     * Next method from onCreate() of activity.
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    }

    /**
     * next to onCreate is onCreateview()
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_map,container, false);
        ButterKnife.bind(this,rootView);
        init();
        return rootView;
    }

    private void init() {
        mRequestingLocationUpdates = false;
        isGestureRecorded = false;
        shouldFetchLandingLocation = false;
        zoomRequire = true;
        mLastUpdateTime = "";
        
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        mSettingsClient = LocationServices.getSettingsClient(context);
        helper = new LocationHelper();
        helper.init(context);
        landingLocations = new ArrayList<>();
        bottomSheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        bottomSheetBehavior.setPeekHeight(0);
        landingMarkers = new ArrayList<>();
        confirm_button = layoutBottomSheet.findViewById(R.id.confirm_button);
        confirm_button.setOnClickListener(v -> bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED));

        activity.setSupportActionBar(toolbarMap);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbarMap.setNavigationIcon(null);

        progressBarMap.setVisibility(View.VISIBLE);
        googleApiAvailability = GoogleApiAvailability.getInstance();
        if(Utility.isGooglePlayServicesAvailable(activity)) {
            FirebaseApp.initializeApp(context);
            registerDeviceForFCM();
        }else{
            int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
            googleApiAvailability.getErrorDialog(activity,status , 2404).show();
        }

        toggle_location_button(true);
        shouldShowProgressLayout(false, "");
        setMapFragment();
        createLocationCallback();
//        createLocationRequest();
        mCurrentLocation = Utility.getDefaultLocation();
        toolbarTitle.setTypeface(null, Typeface.BOLD);
        selectLocationBtn.setTypeface(null, Typeface.BOLD);
        marshMallowPermission = new MarshMallowPermission(activity);
        setPrefsForFirstTime();
    }

    private void setPrefsForFirstTime() {
        preferences.edit().putBoolean(StringConstants.KEY_SHOULD_SHOW_MAP,false).apply();
    }

    private void registerDeviceForFCM() {
        if (FirebaseInstanceId.getInstance() != null) {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(task -> {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        if (task.getResult() != null) {
                            String token = task.getResult().getToken();
                            LogUtils.printLog(TAG, "token = " + token);
                            Utility.saveFCMToken(token);
                            sendRegistrationToServer(token);
                        }
                    });
        }

    }

    private void sendRegistrationToServer(String token) {
        if (token != null) {
            ApiClientInterface apiClientInterface = APIClient.createService(ApiClientInterface.class);
            JsonObject deviceData = new JsonObject();
            deviceData.addProperty("fcm_token", token);
            deviceData.addProperty("platform", getResources().getString(R.string.platform));
            try {
                Call<ResponseBody> fcmResponse = apiClientInterface.registerDeviceFCM("Bearer " + Utility.getJwtToken(), deviceData);
                fcmResponse.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            if (response.code() == 200) {
                                if (response.body() != null) {
                                    JSONObject object = new JSONObject(response.body().string());
                                    LogUtils.printLog(TAG, "object" + object);
                                    Utility.saveFCMToken(token);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        //TODO remove this line from failure
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void shouldShowProgressLayout(boolean showProgress, String progress_text) {
        if (showProgress) {
            progress_layout.setVisibility(View.VISIBLE);
            bottom_location_view.setVisibility(View.GONE);
            locationSelectLayout.setVisibility(View.GONE);
            infoLayout.setVisibility(View.GONE);
            progress_text_view.setText(progress_text);
        } else {
            progress_layout.setVisibility(View.GONE);
            infoLayout.setVisibility(View.VISIBLE);
            bottom_location_view.setVisibility(View.GONE);
            locationSelectLayout.setVisibility(View.VISIBLE);
        }
    }

    private void toggle_location_button(Boolean isDeliveryPossible) {
        if (isDeliveryPossible) {
            outside_btn.setVisibility(View.GONE);
            confirm_pickup_btn.setVisibility(View.VISIBLE);
            selectLocationBtn.setVisibility(View.VISIBLE);
            locationSelectLayout.setVisibility(View.VISIBLE);
            infoLayout.setVisibility(View.VISIBLE);
        } else {
            outside_btn.setVisibility(View.VISIBLE);
            confirm_pickup_btn.setVisibility(View.GONE);
            selectLocationBtn.setVisibility(View.GONE);
            locationSelectLayout.setVisibility(View.GONE);
            infoLayout.setVisibility(View.GONE);
        }
    }


    private void setMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null)
            mapFragment.getMapAsync(this);

//        MapsInitializer.initialize(context);
    }

    private void createLocationRequest() {
        LogUtils.printLog(TAG,"creating location request");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void createLocationCallback() {
        LogUtils.printLog(TAG, "locationCallback");
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mCurrentLocation = locationResult.getLastLocation();
                mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                LogUtils.printLog(TAG, "Location obj = " + mCurrentLocation);

                updateLocationUI(mCurrentLocation);
            }
        };
    }

    private void buildLocationSettingsRequest() {
        LogUtils.printLog(TAG,"build location setting request");
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LogUtils.printLog(TAG, "map ready");
        progressBarMap.setVisibility(View.GONE);
        mMap = googleMap;
        mMap.setOnCameraMoveListener(this);
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        setCameraOnSpecificLatLng();

        try {
            boolean success = mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_style));
            if (!success) {
                Log.e(TAG, "Map Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find Map style. Error: ", e);
        }

        //updating map with default selected location for browsing and ordering
        updateLocationUI(mCurrentLocation);

        mMap.setOnMarkerClickListener(marker -> {
            LogUtils.printLog(TAG, "marker clicked");
            if (marker.getTag() instanceof LandingLocation) {
                LogUtils.printLog(TAG, "marker found = " + marker.getTag());
                setupSelectedLandingMarker(marker);
            }
            return false;
        });

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                View v = LayoutInflater.from(context).inflate(R.layout.marker_info_layout, null);
                TextView title = v.findViewById(R.id.info_title);
                title.setText(marker.getTitle());
                return v;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });
    }

    private void setupSelectedLandingMarker(Marker marker) {
        sourceAddress = ((LandingLocation) marker.getTag()).getAddress();
        current_location.setText(sourceAddress);
        selectedLandingLocation = (LandingLocation) marker.getTag();

        locationSelectLayout.setVisibility(View.VISIBLE);
        selectLocationText.setText("Select " + sourceAddress + " for your delivery? ");
        infoLayout.setVisibility(View.GONE);
    }

    private void setCameraOnSpecificLatLng() {
        if (mMap != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Constants.DEFAULT_LATLNG, 9));
        }
    }

    @Override
    public void onCameraMove() {

    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE && mMap != null) {
            LogUtils.printLog(TAG, "Calling camera move");
            isGestureRecorded = true;
//            mMap.clear();
            if(mCurrLocationMarker!=null){
                mCurrLocationMarker.remove();
            }
            location_img_up.setVisibility(View.VISIBLE);
            mRequestingLocationUpdates = true;
            shouldFetchLandingLocation = true;
            selectedLandingLocation = null;
            location_img_up.setColorFilter(ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void onCameraIdle() {
        if (isGestureRecorded && mRequestingLocationUpdates) {
            LogUtils.printLog(TAG, "Calling cameraIdle");
            location_img_up.setVisibility(View.GONE);

            LatLng targetLatLng = mMap.getCameraPosition().target;
            if (mCurrentLocation != null && targetLatLng != null) {
                LogUtils.printLog(TAG,"camera idle not null");
                mCurrentLocation.setLatitude(targetLatLng.latitude);
                mCurrentLocation.setLongitude(targetLatLng.longitude);
                updateLocationUI(mCurrentLocation);
            }
            mRequestingLocationUpdates = false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    Log.i(TAG, "User agreed to make required location settings changes.");
                    startLocationUpdates();
                    break;
                case Activity.RESULT_CANCELED:
                    Log.i(TAG, "User chose not to make required location settings changes.");
                    mRequestingLocationUpdates = false;
                    showSnackbarWithAction(coordinate_map, "Please approve the location service in order to find a pickup point.", getResources().getString(R.string.ok),true);
                    break;
            }
        }
    }

    //
    private void showSnackbarWithAction(View coordinateView, String s, String actionString, boolean shouldLocationUpdate) {
        Snackbar snackbar = Snackbar
                .make(coordinateView, s, Snackbar.LENGTH_INDEFINITE)
                .setAction(actionString, view -> {
                    if(shouldLocationUpdate)
                        startLocationUpdates();
                });

        View view = snackbar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(getResources().getColor(R.color.white));
        snackbar.show();
    }


    @OnClick(R.id.cancel_button)
    void finishActivity() {
        Navigation.findNavController(rootView).navigateUp();
    }

//    @OnClick(R.id.confirm_pickup_btn)
//    void goToRestaurantActivity() {
//        if (mCurrentLocation != null) {
//            if (selectedLandingLocation != null) {
//                LogUtils.printLog(TAG, "Pickup location is = " + selectedLandingLocation);
//                cartSingleton.setSelectedLandingLocation(selectedLandingLocation);
//                Intent intent = new Intent(MapActivity.this, RestaurantActivity.class);
//                startActivity(intent);
//                this.activity.finish();
//            } else {
//                showSnackbar("Please select one of the predefined landing locations.");
//            }
//        }
//    }


    @OnClick(R.id.select_location_btn)
    void goToRestaurantActivity() {
        if (mCurrentLocation != null) {
            if (selectedLandingLocation != null) {
                LogUtils.printLog(TAG, "Pickup location is = " + selectedLandingLocation);
                cartSingleton.setSelectedLandingLocation(selectedLandingLocation);
                Navigation.findNavController(rootView).navigate(R.id.action_map_to_restaurant_fragment);
            } else {
                showSnackbar("Please select one of the predefined landing locations.");
            }
        }
    }

    @OnClick(R.id.fab_map_type)
    void changeMapType() {
        if (mMap != null) {
            if (mMap.getMapType() == 1) {
                mMap.setMapType(4);
            } else {
                mMap.setMapType(1);
            }
        }
    }


    private void getLandingLocations(Double lat, Double lng) {
        try {
            shouldShowProgressLayout(true, "Fetching nearby landing locations");
            landingLocations = new ArrayList<>();
            Call<ResponseBody> landingLocationResponse = apiClientInterface.getLandingLocations("Bearer " + Utility.getJwtToken(), lat, lng);
            landingLocationResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    LogUtils.printLog(TAG, "code =" + response.code());
                    try {
                        if (response.code() == 200 || response.code() == 201) {
                            if (response.body() != null) {
                                JSONArray array = new JSONArray(response.body().string());
                                LogUtils.printLog(TAG, "response = " + array);
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jo = array.getJSONObject(i);
                                    int id = jo.getInt("id");
                                    JSONObject location_obj = jo.getJSONObject("location");
                                    double lat = location_obj.getJSONObject("coordinates").getDouble("lat");
                                    double lng = location_obj.getJSONObject("coordinates").getDouble("lng");
                                    String address = location_obj.getString("address");
                                    String distance = jo.getString("distance");
                                    LandingLocation landingLocation = new LandingLocation(id, lat, lng, address, distance);
                                    landingLocations.add(landingLocation);
                                }
                            }
                            shouldShowProgressLayout(false, "");
                            LatLng userLatLng = new LatLng(lat, lng);
                            setupLandingLocationsOnUi(userLatLng);
                        } else {
                            if (response.errorBody() != null) {
                                JSONObject object = new JSONObject(response.errorBody().string());
                                String message = object.getString("error");
                                Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    shouldShowProgressLayout(false, "");
                    showSnackbar("Network Error, Please try again later");

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setupLandingLocationsOnUi(LatLng userLatLng) {
        if (landingLocations.size() == 0) {
            toggleBottomSheet();
        } else {
            toggle_location_button(true);
            landingMarkers.clear();
            markerOptions = new MarkerOptions();
            Marker landingMarker;
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (LandingLocation landingLocation : landingLocations) {
                LatLng markerLatLng = new LatLng(landingLocation.getmLatitude(), landingLocation.getmLongitude());
                markerOptions.position(markerLatLng);
                landingMarker = mMap.addMarker(markerOptions);
                landingMarker.setTag(landingLocation);
                landingMarker.setTitle(landingLocation.getAddress());
                landingMarker.setIcon(bitmapDescriptorFromVector(context));
                landingMarkers.add(landingMarker);
                builder.include(markerLatLng);
            }
            LogUtils.printLog(TAG,"landing Markers size = "+landingMarkers.size());
            builder.include(userLatLng);
            LatLngBounds bounds = builder.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.15); // offset from edges of the map 10% of screen
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
//            mMap.animateCamera(cameraUpdate);

            calculateDistanceToCurrentlocation(mCurrentLocation,landingLocations);
        }
    }

    private void calculateDistanceToCurrentlocation(Location mCurrentLocation, ArrayList<LandingLocation> landingLocations) {
        if(mCurrentLocation!=null && landingLocations.size()>0){
            Location location = new Location("");
            for (int i = 0;i<landingLocations.size();i++) {
                location.setLatitude(landingLocations.get(i).getmLatitude());
                location.setLongitude(landingLocations.get(i).getmLongitude());
                if(Math.abs(mCurrentLocation.distanceTo(location)) < 25){
                    setupSelectedLandingMarker(landingMarkers.get(i));
                }
            }
        }
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_polygon);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());

        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    public void toggleBottomSheet() {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
        toggle_location_button(false);
    }

    @OnClick(R.id.explore_delivey_area)
    void exploreDeliveryArea() {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        updateLocationUI(Utility.getDefaultLocation());
    }


    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    @OnClick(R.id.fab_current_location)
    void startLocationUpdates() {
        if(checkPermissions()){
            LogUtils.printLog(TAG,"permission given");
            if(mLocationRequest == null)
                createLocationRequest();
            if(mLocationSettingsRequest == null)
                buildLocationSettingsRequest();
            shouldShowProgressLayout(true, "Fetching Current Location...");
            mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                    .addOnSuccessListener(activity, locationSettingsResponse -> {
                        Log.i(TAG, "All location settings are satisfied.");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED
                                    && activity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED) {
                                requestPermissions();
                            } else {
                                LogUtils.printLog(TAG, "Location permission Satisfied.");
                            }
                        }
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                        mRequestingLocationUpdates = true;
                    })
                    .addOnFailureListener(activity, e -> {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(activity, REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);
                                Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
                                mRequestingLocationUpdates = false;
                        }
                        shouldShowProgressLayout(false, "");
                    });
        }else {
            requestingCurrentLocation = true;
            requestPermissions();
        }
    }
//    void startLocationUpdates() {
//        buildLocationSettingsRequest();
//        checkForPermissionAndSetting();
//        if(mLocationRequest == null)
//            createLocationRequest();
//        shouldShowProgressLayout(true, "Fetching Current Location...");
//        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
//                .addOnSuccessListener(this, locationSettingsResponse -> {
//                    Log.i(TAG, "All location settings are satisfied.");
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                            requestPermissions();
//                        } else {
//                            LogUtils.printLog(TAG, "Location permission Satisfied.");
//                        }
//                    }
//                    mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
//                    mRequestingLocationUpdates = true;
//                })
//                .addOnFailureListener(this, e -> {
//                    int statusCode = ((ApiException) e).getStatusCode();
//                    switch (statusCode) {
//                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
//                                    "location settings ");
//                            try {
//                                // Show the dialog by calling startResolutionForResult(), and check the
//                                // result in onActivityResult().
//                                ResolvableApiException rae = (ResolvableApiException) e;
//                                rae.startResolutionForResult(MapActivity.this, REQUEST_CHECK_SETTINGS);
//                            } catch (IntentSender.SendIntentException sie) {
//                                Log.i(TAG, "PendingIntent unable to execute request.");
//                            }
//                            break;
//                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                            String errorMessage = "Location settings are inadequate, and cannot be " +
//                                    "fixed here. Fix in Settings.";
//                            Log.e(TAG, errorMessage);
//                            Toast.makeText(MapActivity.this, errorMessage, Toast.LENGTH_LONG).show();
//                            mRequestingLocationUpdates = false;
//                    }
//                    shouldShowProgressLayout(false, "");
//                });
//    }

    private void checkForPermissionAndSetting() {
        if (checkPermissions()) {
            LogUtils.printLog(TAG,"checkPermission is done in Resume");
            startLocationUpdates();
        } else if (!checkPermissions()) {
            LogUtils.printLog(TAG,"checkPermission is not done");
            requestPermissions();
        }
    }

    /**
     * Sets the value of the UI fields for the location latitude, longitude and last update time.
     *
     * @param mCurrentLocation
     */
    private void updateLocationUI(Location mCurrentLocation) {
        shouldShowProgressLayout(false, "");
        if (mCurrentLocation != null) {
            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }
            LogUtils.printLog(TAG, "updatedLocation lat = " + mCurrentLocation.getLatitude() + " & lng = " + mCurrentLocation.getLongitude());
            sourceAddress = helper.getAddress(mCurrentLocation);
            current_location.setText(sourceAddress);
            moveMarkerOnMap(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
            stopLocationUpdates();
            getLandingLocations(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            zoomRequire = false;
        }
    }

    private void moveMarkerOnMap(LatLng currentLocation) {
        markerOptions = new MarkerOptions();
        markerOptions.position(currentLocation);
        markerOptions.anchor(0.5f, 0.5f);

        LogUtils.printLog(TAG, "LatLng = " + currentLocation);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
        if (zoomRequire) {
            Handler handler = new Handler();
            handler.postDelayed(() -> mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15)), 1000);
        }

        mCurrLocationMarker = mMap.addMarker(markerOptions);
        mCurrLocationMarker.setTitle("My location");
        if (mMap != null && mMap.getMapType() == 2) {
            mCurrLocationMarker.setIcon(Utility.bitmapDescriptorFromVector(context, R.drawable.ic_person_pin_24px));
        } else {
            mCurrLocationMarker.setIcon(Utility.bitmapDescriptorFromVector(context, R.drawable.ic_person_pin_24px));
        }
    }

    /**
     * Removes location updates from the FusedLocationApi.
     */
    private void stopLocationUpdates() {
        if (!mRequestingLocationUpdates) {
            LogUtils.printLog(TAG, "stopLocationUpdates: updates never requested, no-op.");
            return;
        }
        if (mFusedLocationClient != null && mLocationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(activity, task -> mRequestingLocationUpdates = false);
        }
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState!=null) {
            savedInstanceState.putBoolean(KEY_REQUESTING_LOCATION_UPDATES, mRequestingLocationUpdates);
            savedInstanceState.putParcelable(KEY_LOCATION, mCurrentLocation);
            savedInstanceState.putString(KEY_LAST_UPDATED_TIME_STRING, mLastUpdateTime);

            super.onSaveInstanceState(savedInstanceState);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBarMap.setVisibility(View.GONE);
        if(requestingCurrentLocation){
            startLocationUpdates();
        }
        // Within {@code onPause()}, we remove location updates. Here, we resume receiving
        // location updates if the user has requested them.
//        LogUtils.printLog(TAG,"RequestLocationUpdate = "+mRequestingLocationUpdates);
//        if (checkPermissions()) {
//            LogUtils.printLog(TAG,"checkPermission is done in Resume");
//            startLocationUpdates();
//        } else if (!checkPermissions()) {
//            LogUtils.printLog(TAG,"checkPermission is not done");
//            requestPermissions();
//        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    public void onStart() {
        super.onStart();
        mRequestingLocationUpdates = true;
    }

    private void showSnackbar(final String mainTextStringId) {
        Snackbar.make(coordinate_map, mainTextStringId, Snackbar.LENGTH_LONG).show();
    }

    /**
     * Method for checking ACCESS_FINE_LOCATION permission
     *
     * Below methods are for request and result of permission.
     * @return
     */
    private boolean checkPermissions() {
        int permissionState = context.checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION);
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
        } else {
            if (firstTime) {
                firstTime = false;
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        REQUEST_PERMISSIONS_REQUEST_CODE);
            } else {
                LogUtils.printLog(TAG, "never ask again");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        LogUtils.printLog(TAG,"request Code = "+requestCode);
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (mRequestingLocationUpdates) {
                    Log.i(TAG, "Permission granted, updates requested, starting location updates");
                    startLocationUpdates();
                }
            } else {
                LogUtils.printLog(TAG, "Permission result -> Not granted");
                showSnackbarWithAction(coordinate_map, getResources().getString(R.string.location_necessary_text), getResources().getString(R.string.ok),false);
            }
        }
    }
}