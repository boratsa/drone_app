package com.bydrone.droneapplication.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bydrone.droneapplication.Interface.OnItemUpdateListener;
import com.bydrone.droneapplication.Interface.StickyHeaderItemDecorationInterface;
import com.bydrone.droneapplication.Models.Cart;
import com.bydrone.droneapplication.Models.Category;
import com.bydrone.droneapplication.Models.FoodItem;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.CartAddButton;
import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.Utils.LogUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FoodItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyHeaderItemDecorationInterface {
    private String TAG = FoodItemsAdapter.class.getSimpleName();
    private ArrayList<FoodItem> foodItems = new ArrayList<>();
    private ArrayList<Category> categories = new ArrayList<>();
    private ArrayList<Object> dataList = new ArrayList<>();
    private Context context;
    private OnItemUpdateListener onItemUpdateListener;
    private HashMap<Integer, Integer> categoryPositionMap = new HashMap<>();
    private CartSingleton cartSingleton;
    private String vendorEmail = "";
    private String vendorContact = "";
    public FoodItemsAdapter(ArrayList<FoodItem> foodItems, Context context, OnItemUpdateListener onItemUpdateListener,
                            ArrayList<Category> categories, CartSingleton cartSingleton,
                            String vendorEmail, String vendorContact) {
        this.context = context;
        this.foodItems = foodItems;
        this.categories = categories;
        this.onItemUpdateListener = onItemUpdateListener;
        this.cartSingleton = cartSingleton;
        this.vendorContact = vendorContact;
        this.vendorEmail = vendorEmail;
        setAdapterData();
    }

    private void setAdapterData() {
        dataList.add("info");
        int pos = 1;
        for (Category category : categories) {
            dataList.add(category);
            categoryPositionMap.put(category.getId(), pos);
            pos = pos + 1;
            dataList.addAll(category.getItems());
            pos = pos + category.getItems().size();
        }

        LogUtils.printLog(TAG, String.valueOf(dataList));
    }

    /**
     * viewType = 0 i.e. Category, viewType=1 i.e. FoodItem, viewType=2 i.e. info
     *
     * @param parent
     * @param viewType
     * @return
     */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        switch (viewType) {
            case 0:
                View headerView = inflater.inflate(R.layout.food_item_header, parent, false);
                return new FoodItemsAdapter.HeaderViewHolder(headerView);
            case 2:
                View infoView = inflater.inflate(R.layout.restaurant_info_layout, parent, false);
                return new FoodItemsAdapter.InfoViewHolder(infoView);
            default:
                View restaurantCardView = inflater.inflate(R.layout.food_items_card, parent, false);
                return new FoodItemsAdapter.ItemViewHolder(restaurantCardView);
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ItemViewHolder) {
            ItemViewHolder holder = ((ItemViewHolder) viewHolder);
            FoodItem foodItem = (FoodItem) dataList.get(position);
            TextView textView = holder.foodItemName;
            textView.setText(String.valueOf(foodItem.getName()));
            textView.setTypeface(null, Typeface.BOLD);

            holder.foodItemPrice.setText(String.valueOf(foodItem.getPrice()) + context.getResources().getString(R.string.euro));
            holder.foodItemWeight.setText("~" + String.valueOf(foodItem.getWeight()) + "g");
            Glide.with(context).load(foodItem.getImageUrlMedium()).into(holder.foodItemImage);

            if (!cartSingleton.getCart().isEmpty()) {
                Cart cart = cartSingleton.getCart();
                int quantity = cart.getFoodItemQuantity(foodItem.getId());
                holder.cartAddButton.setCount(quantity);
            }

            holder.cartAddButton.setItemChangedListener((itemCount ,buttonType)-> {
                LogUtils.printLog(TAG,"addButton itemCount = "+itemCount);
                try {
                    if (cartSingleton.getCart().isRestaurantChange(foodItem)) {
                        new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setTitle("You can only order from one restaurant at a time")
                                .setMessage("Clear previous cart if you'd still like to add this item to your cart")
                                .setPositiveButton("Clear cart & Add", (dialog, which) -> {
                                    cartSingleton.clearCartAndSetRestuarantId(foodItem.getRestaurant());
                                    updateMenuItems(foodItem, itemCount);
                                }).setNegativeButton("Cancel", (dialog, which) -> {
                                    holder.cartAddButton.setCount(0);
                                    cartSingleton.revertSelectedRestaurant();
                                }).create().show();
                    } else {
                        if(buttonType==1) {
                            if (!cartSingleton.getCart().cartMaxWeightReached(foodItem, itemCount)) {
                                updateMenuItems(foodItem, itemCount);
                            } else {
                                holder.cartAddButton.setCount(itemCount - 1);
                                onItemUpdateListener.onMaxWeightReached();
                            }
                        }else{
                            updateMenuItems(foodItem, itemCount);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        } else if (viewHolder instanceof HeaderViewHolder) {
            Category category = (Category) dataList.get(position);
            String catName = category.getName();
            ((HeaderViewHolder) viewHolder).headerText.setText(catName);
            ((HeaderViewHolder) viewHolder).headerText.setTypeface(null, Typeface.BOLD);
        } else if (viewHolder instanceof InfoViewHolder) {
            if(foodItems.size()>0) {
                ((InfoViewHolder) viewHolder).restaurantName.setText(foodItems.get(0).getRestaurant().getName());
                ((InfoViewHolder) viewHolder).restaurantContact.setText(vendorContact);
                ((InfoViewHolder) viewHolder).restaurantWebsite.setText(vendorEmail);
                String openingHrs = "";
                if (foodItems.get(0).getRestaurant().getName().toLowerCase().contains("fafa")) {
                    openingHrs = "Mon-Sat : 10:30-21:00 \nSun : 11:00-21:00";
                } else if (foodItems.get(0).getRestaurant().getName().toLowerCase().contains("market")) {
                    openingHrs = "Mon-Fri : 07:00-23:00 \nSat : 09:00-23:00 \nSun : 10:00-21:00";
                } else if (foodItems.get(0).getRestaurant().getName().toLowerCase().contains("brooklyn")) {
                    openingHrs = "Mon-Thu : 08:00-18:00 \nFri : 08:00-16:00 \nSat-Sun : Closed";
                } else if (foodItems.get(0).getRestaurant().getName().toLowerCase().contains("little")) {
                    openingHrs = "Mon-Fri : 10:00-18:00 \nSat : 10:00-16:00 \nSun : Closed";
                }

                ((InfoViewHolder) viewHolder).restaurantOpeningHrs.setText(openingHrs);
            }

        }
    }

    private void updateMenuItems(FoodItem foodItem, int itemCount) {
        cartSingleton.getCart().updateFoodItem(foodItem, itemCount);
        setUpdateListener(cartSingleton.getCart().getFoodItems());
        cartSingleton.getCart().printItems();
    }


    private void setUpdateListener(ArrayList<FoodItem> items) {
        onItemUpdateListener.onItemUpdate();
    }

    public HashMap<Integer, Integer> getCategoryPositionMap() {
        return categoryPositionMap;
    }

    public int getCategoryItemPosition(int id) {
        if (categoryPositionMap != null && categoryPositionMap.containsKey(id))
            return categoryPositionMap.get(id);
        return -1;
    }

    public int getCategoryItemId(int position) {
        if (categoryPositionMap.containsValue(position)) {
            for (Map.Entry entry : categoryPositionMap.entrySet()) {
                if (position == (Integer) entry.getValue()) {
                    return (Integer) entry.getKey();
                }
            }
        }
        return -1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 2;
        }

        if (dataList.get(position) instanceof Category) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void updateItemCount() {
        notifyDataSetChanged();
    }

    @Override
    public int getHeaderPositionForItem(int itemPosition) {
        int headerPosition = 1;
        do {
            if (this.isHeader(itemPosition)) {
                headerPosition = itemPosition;
                break;
            }
            itemPosition -= 1;
        } while (itemPosition >= 0);
        return headerPosition;
    }

    @Override
    public int getHeaderLayout(int headerPosition) {
        if (dataList.get(headerPosition) instanceof Category)
            return R.layout.food_item_header;
        else
            return R.layout.food_items_card;
    }

    @Override
    public void bindHeaderData(View header, int headerPosition) {
        String catName = ((Category) dataList.get(headerPosition)).getName();
        ((TextView) header.findViewById(R.id.header_text)).setText(catName);
        ((TextView) header.findViewById(R.id.header_text)).setTypeface(null, Typeface.BOLD);
    }

    @Override
    public boolean isHeader(int itemPosition) {
        return dataList.get(itemPosition) instanceof Category;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.foodItemName)
        TextView foodItemName;
        @BindView(R.id.foodItemPrice)
        TextView foodItemPrice;
        @BindView(R.id.foodItemWeight)
        TextView foodItemWeight;
        @BindView(R.id.foodMenuCardItem)
        CardView foodMenuCardItem;
        @BindView(R.id.cartAddButton)
        CartAddButton cartAddButton;
        @BindView(R.id.imageView)
        ImageView foodItemImage;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.header_text)
        TextView headerText;

        HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class InfoViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.restaurant_name)
        TextView restaurantName;
        @BindView(R.id.restaurant_contact)
        TextView restaurantContact;
        @BindView(R.id.restaurant_website)
        TextView restaurantWebsite;
        @BindView(R.id.restaurant_opening_hrs)
        TextView restaurantOpeningHrs;
        InfoViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}