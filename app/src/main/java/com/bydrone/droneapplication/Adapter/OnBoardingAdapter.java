package com.bydrone.droneapplication.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.viewpager.widget.PagerAdapter;

import com.bydrone.droneapplication.Interface.OnBoardingItemClickListener;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.Constants;

import java.util.regex.Pattern;

import butterknife.OnClick;

public class OnBoardingAdapter extends PagerAdapter {
    private Context context;
    private LayoutInflater mLayoutInflater;
    private View itemView;
    private OnBoardingItemClickListener onWalkThroughItemClickListener;

    public OnBoardingAdapter(Context mContext, OnBoardingItemClickListener onWalkThroughItemClickListener) {
        this.context = mContext;
        this.onWalkThroughItemClickListener = onWalkThroughItemClickListener;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == o;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        itemView = mLayoutInflater.inflate(R.layout.splash_pager_item, container, false);
        TextView walkthroughTitle = itemView.findViewById(R.id.walktrough_title);
        TextView walkthroughSubTitle = itemView.findViewById(R.id.walktrough_subtitle);
        TextView walkthroughBottomText = itemView.findViewById(R.id.walkthrough_bottom_text);
        ImageView walkthroughImage = itemView.findViewById(R.id.walktrough_image);
        RelativeLayout button_layout = itemView.findViewById(R.id.button_layout);
        Button ask_notification_btn = itemView.findViewById(R.id.ask_notification_btn);
        TextView not_now_btn = itemView.findViewById(R.id.not_now_btn);

        String textTitle = "";
        String textSubTitle = "";
        CharSequence textBottom = "";
        switch (position) {
            case 0:
                button_layout.setVisibility(View.INVISIBLE);
                textTitle = context.getResources().getString(R.string.walkthrough_title_1);
                textSubTitle = context.getResources().getString(R.string.walkthrough_subtitle_1);
                ;
                textBottom = context.getResources().getString(R.string.walkthrough_bottom_1);
                break;
            case 1:
                button_layout.setVisibility(View.VISIBLE);
                textTitle = context.getResources().getString(R.string.walkthrough_title_2);
                textSubTitle = "";
                textBottom = context.getResources().getString(R.string.walkthrough_bottom_2);
                break;
            case 2:
                button_layout.setVisibility(View.VISIBLE);
                textTitle = context.getResources().getString(R.string.walkthrough_title_3);
                textSubTitle = "";
                textBottom = context.getResources().getString(R.string.walkthrough_bottom_3);
                ask_notification_btn.setText(context.getResources().getString(R.string.allow_notifications));
                ask_notification_btn.setOnClickListener(null);
                not_now_btn.setVisibility(View.INVISIBLE);
                break;
            case 3:
                walkthroughBottomText.setTextSize((float) 15.0);
                walkthroughBottomText.setTypeface(Typeface.DEFAULT);
                button_layout.setVisibility(View.VISIBLE);
                not_now_btn.setVisibility(View.INVISIBLE);
                ask_notification_btn.setText(context.getResources().getString(R.string.get_started));
                textSubTitle = context.getResources().getString(R.string.walkthrough_subtitle_4);
                ;
                textTitle = context.getResources().getString(R.string.walkthrough_title_4);

                String conditionOfUse = context.getResources().getString(R.string.walkthrough_condition_of_use);
                String privacyPolicy = context.getResources().getString(R.string.walkthrough_privacy_policy);

                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder
                        (context.getResources().getString(R.string.walkthrough_bottom_4));
                spannableStringBuilder.append(" ");
                spannableStringBuilder.append(conditionOfUse);
                spannableStringBuilder.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View v) {
                        startPrivacyLink();
                    }
                }, spannableStringBuilder.length() - conditionOfUse.length(),
                        spannableStringBuilder.length(), 0);
                spannableStringBuilder.append(" and \n");
                spannableStringBuilder.append(privacyPolicy);
                spannableStringBuilder.setSpan(new ClickableSpan() {
                    @Override
                    public void onClick(View v) {
                        startPrivacyLink();
                    }
                }, spannableStringBuilder.length() - privacyPolicy.length(), spannableStringBuilder.length(), 0);
                walkthroughBottomText.setMovementMethod(LinkMovementMethod.getInstance());
                textBottom = spannableStringBuilder;
                break;
        }

        ask_notification_btn.setOnClickListener(v -> onClickButton(position, "notification"));
        not_now_btn.setOnClickListener(v -> onClickButton(position, "never"));
        walkthroughBottomText.setText(textBottom);
        walkthroughTitle.setText(textTitle);
        walkthroughSubTitle.setText(textSubTitle);
        walkthroughTitle.setTypeface(null, Typeface.BOLD);
        walkthroughBottomText.setTypeface(null, Typeface.BOLD);
        walkthroughImage.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_bydrone_icon));
        container.addView(itemView);
        return itemView;
    }

    private void startPrivacyLink() {
        Intent privacyIntent= new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.BYDRONE_URL));
        context.startActivity(privacyIntent);
    }

    private void onClickButton(int position, String notification) {
        onWalkThroughItemClickListener.onClick(position, notification);
    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

}
