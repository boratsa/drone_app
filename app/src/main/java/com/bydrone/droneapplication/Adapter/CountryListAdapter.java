package com.bydrone.droneapplication.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bydrone.droneapplication.Interface.OnCountryItemClickListener;
import com.bydrone.droneapplication.R;

import java.util.ArrayList;

/**
 * @author Ankur Khandelwal on 2019-09-12.
 */
public class CountryListAdapter extends ArrayAdapter<String> {
    private final Context context;
    private final ArrayList<String> countryNameList;
    private final ArrayList<String> countryCodeList;
    private OnCountryItemClickListener itemClickListener;
    public CountryListAdapter(Context context, ArrayList<String> countryName, ArrayList<String> countryCode, OnCountryItemClickListener itemClickListener) {
        super(context, R.layout.country_list_item, countryName);
        this.context = context;
        this.countryCodeList = countryCode;
        this.countryNameList = countryName;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.country_list_item, parent, false);
        TextView countryName = (TextView) rowView.findViewById(R.id.country_name);
        TextView countryCode = (TextView) rowView.findViewById(R.id.country_code);
        String name = countryNameList.get(position);
        String code = countryCodeList.get(position);
        countryName.setText(name);
        countryCode.setText(code);

        rowView.setOnClickListener(v->{
            itemClickListener.onItemClick(name,code);
        });
        return rowView;
    }
}