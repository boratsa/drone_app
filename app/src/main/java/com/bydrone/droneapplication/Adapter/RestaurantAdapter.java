package com.bydrone.droneapplication.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bydrone.droneapplication.Interface.OnRestaurantClickListener;
import com.bydrone.droneapplication.Models.Restaurant;
import com.bydrone.droneapplication.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RestaurantAdapter extends RecyclerView.Adapter<RestaurantAdapter.ViewHolder> {
    private ArrayList<Restaurant> restaurantsList;
    private Context context;
    private OnRestaurantClickListener onRestaurantClickListener;

    public RestaurantAdapter(ArrayList<Restaurant> restaurants, Context context, OnRestaurantClickListener onRestaurantClickListener) {
        this.restaurantsList = restaurants;
        this.context = context;
        this.onRestaurantClickListener = onRestaurantClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View restaurantCardView = inflater.inflate(R.layout.restaurants_card, parent, false);
        return new ViewHolder(restaurantCardView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Restaurant restaurant = restaurantsList.get(position);

        CardView restaurantCardItem = viewHolder.restaurantCardItem;
        viewHolder.restaurantName.setText(restaurant.getName());
        viewHolder.restaurantName.setTypeface(null, Typeface.BOLD);

        Glide.with(context).load(restaurant.getImage().getMediumSquareCrop()).into(viewHolder.imageView);
        if (!restaurant.getIsOpen()) {
            viewHolder.delivery_time.setText(context.getResources().getString(R.string.closed_for_order));
            viewHolder.deliveryTimeImage.setBackground(context.getResources().getDrawable(R.drawable.ic_rest_closed));
            viewHolder.imageView.setAlpha((float) 0.2);
            viewHolder.moderatorDeliveryTime.setVisibility(View.GONE);
        }else{
            viewHolder.moderatorDeliveryTime.setVisibility(View.VISIBLE);
        }
        viewHolder.restaurantSpeciality.setText(restaurant.getSpeciality());

        restaurantCardItem.setOnClickListener(v -> {
            if (restaurant.getIsOpen()) {
                ViewCompat.setTransitionName(viewHolder.imageView,restaurant.getName());
                onRestaurantClickListener.onRestaurantClick(restaurant,viewHolder.imageView);
            }
        });

    }

    @Override
    public int getItemCount() {
        return restaurantsList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.restaurantName)
        TextView restaurantName;
        @BindView(R.id.restaurantCardItem)
        CardView restaurantCardItem;
        @BindView(R.id.delivery_time)
        TextView delivery_time;
        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.restaurantSpeciality)
        TextView restaurantSpeciality;
        @BindView(R.id.timer_img)
        ImageView deliveryTimeImage;
        @BindView(R.id.moderator_delivery_time)
        TextView moderatorDeliveryTime;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
