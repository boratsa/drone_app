package com.bydrone.droneapplication.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bydrone.droneapplication.Interface.OnItemUpdateListener;
import com.bydrone.droneapplication.Models.Cart;
import com.bydrone.droneapplication.Models.FoodItem;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.CartAddButton;
import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.Utils.LogUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CartItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private String TAG = CartItemAdapter.class.getSimpleName();
    private ArrayList<FoodItem> foodItems = new ArrayList<>();
    private Context context;
    private OnItemUpdateListener onItemUpdateListener;
    private CartSingleton cartSingleton;
    public CartItemAdapter(ArrayList<FoodItem> foodItems, Context context, OnItemUpdateListener onItemUpdateListener, CartSingleton cartSingleton) {
        this.context = context;
        this.foodItems = foodItems;
        this.onItemUpdateListener = onItemUpdateListener;
        this.cartSingleton = cartSingleton;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View restaurantCardView = inflater.inflate(R.layout.food_items_card, parent, false);
        return new CartItemAdapter.ItemViewHolder(restaurantCardView);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ItemViewHolder) {
            ItemViewHolder holder = ((ItemViewHolder) viewHolder);
            FoodItem foodItem = foodItems.get(position);

            TextView textView = holder.foodItemName;
            textView.setText(String.valueOf(foodItem.getName()));
            textView.setTypeface(null, Typeface.BOLD);

            holder.foodItemPrice.setText(String.valueOf(foodItem.getPrice()) + context.getResources().getString(R.string.euro));
            holder.foodItemWeight.setText("~" + String.valueOf(foodItem.getWeight()) + "g");
            Glide.with(context).load(foodItem.getImageUrlMedium()).into(holder.foodItemImage);

            if (!cartSingleton.getCart().isEmpty()) {
                Cart cart = cartSingleton.getCart();
                int quantity = cart.getFoodItemQuantity(foodItem.getId());
                holder.cartAddButton.setCount(quantity);
            }

            holder.cartAddButton.setItemChangedListener((itemCount ,buttonType)-> {
                try {
                    if (cartSingleton.getCart().isRestaurantChange(foodItem)) {
                        new AlertDialog.Builder(context)
                                .setCancelable(false)
                                .setTitle("You can only order from one restaurant at a time")
                                .setMessage("Clear previous cart if you'd still like to add this item to your cart")
                                .setPositiveButton("Clear cart & Add", (dialog, which) -> {
                                    cartSingleton.clearCartAndSetRestuarantId(foodItem.getRestaurant());
                                    updateMenuItems(foodItem, itemCount);
                                })
                                .setNegativeButton("Cancel", (dialog, which) -> {
                                    holder.cartAddButton.setCount(0);
                                    cartSingleton.revertSelectedRestaurant();
                                }).create().show();
                    } else {
                        if(buttonType==1) {
                            if (!cartSingleton.getCart().cartMaxWeightReached(foodItem, itemCount)) {
                                updateMenuItems(foodItem, itemCount);
                            } else {
                                holder.cartAddButton.setCount(itemCount - 1);
                                onItemUpdateListener.onMaxWeightReached();
                            }
                        }else{
                            updateMenuItems(foodItem, itemCount);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
        }
    }

    private void updateMenuItems(FoodItem foodItem, int itemCount) {
        LogUtils.printLog(TAG, "updatemenu with count =" + itemCount);
        cartSingleton.getCart().updateFoodItem(foodItem, itemCount);
        setUpdateListener(cartSingleton.getCart().getFoodItems());
        cartSingleton.getCart().printItems();
    }


    private void setUpdateListener(ArrayList<FoodItem> items) {
        onItemUpdateListener.onItemUpdate();
    }

    @Override
    public int getItemViewType(int position) {
        LogUtils.printLog(TAG, "itemViewType pos = " + position);
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return foodItems.size();
    }

    public void updateItemCount() {
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.foodItemName)
        TextView foodItemName;
        @BindView(R.id.foodItemPrice)
        TextView foodItemPrice;
        @BindView(R.id.foodItemWeight)
        TextView foodItemWeight;
        @BindView(R.id.foodMenuCardItem)
        CardView foodMenuCardItem;
        @BindView(R.id.cartAddButton)
        CartAddButton cartAddButton;
        @BindView(R.id.imageView)
        ImageView foodItemImage;
        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }

    class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.header_text) TextView headerText;
        HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}