package com.bydrone.droneapplication.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bydrone.droneapplication.Interface.OnOrderItemClickListener;
import com.bydrone.droneapplication.Models.FoodItem;
import com.bydrone.droneapplication.Models.Order;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.Utils.Utility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Ankur Khandelwal on 2019-08-06.
 */
public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {
    private String TAG = OrdersAdapter.class.getSimpleName();
    private ArrayList<Order> mOrders;
    private Context context;
    private OnOrderItemClickListener onOrderItemClickListener;
    private CartSingleton cartSingleton;

    public OrdersAdapter(ArrayList<Order> orders, Context context, OnOrderItemClickListener onOrderItemClickListener, CartSingleton cartSingleton) {
        this.context = context;
        mOrders = orders;
        this.onOrderItemClickListener = onOrderItemClickListener;
        this.cartSingleton = cartSingleton;
    }

    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View orderView = LayoutInflater.from(context).inflate(R.layout.single_order_item, parent, false);
        OrdersAdapter.ViewHolder viewHolder = new OrdersAdapter.ViewHolder(orderView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(OrdersAdapter.ViewHolder viewHolder, int position) {
        Order order = mOrders.get(position);
        viewHolder.restaurantName.setText(order.getSourceLocation().getName());
//        viewHolder.restaurantLocation.setText(order.getSource_address());
        viewHolder.deliveryLocation.setText(order.getDestinationLocation().getAddress());
        String itemText = "";

        viewHolder.restaurantName.setTypeface(null, Typeface.BOLD);
        viewHolder.itemsDescription.setTypeface(null, Typeface.BOLD);
        viewHolder.deliveryLocation.setTypeface(null, Typeface.BOLD);
        viewHolder.date.setTypeface(null, Typeface.BOLD);
        viewHolder.totalAmount.setTypeface(null, Typeface.BOLD);

        for (FoodItem item : order.getGoods().getItems()) {
            itemText += item.getQuantity() + " x " + item.getName() + " , ";
        }
        if (itemText.length() > 0) {
            itemText = itemText.substring(0, itemText.length() - 2);
        }

        Date date = Utility.getDateFromString(order.getDate());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String calendarDay = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
        int calendarMonth = calendar.get(Calendar.MONTH);
        String calendarYear = String.valueOf(calendar.get(Calendar.YEAR));
        String calendarHour = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String calendarMin = String.valueOf(calendar.get(Calendar.MINUTE));
        if (calendar.get(Calendar.MINUTE) < 10) {
            calendarMin = "0" + calendarMin;
        }
        String calendarMonthName = Utility.getMonthName(calendarMonth);
        if (date != null) {
            String dateStr = calendarDay + "-" + calendarMonthName + "-" + calendarYear + "  " + calendarHour + ":" + calendarMin + ":00 EEST";
            viewHolder.date.setText(dateStr);
        }
        viewHolder.itemsDescription.setText(itemText);
        int status = order.getStatus();
        String orderStatusString = cartSingleton.getOrderStatusString(status);
        viewHolder.orderStatus.setText(orderStatusString);

        if (order.getStatus() <= 7 && order.getStatus() >= 0) {
            viewHolder.trackOrderBtn.setVisibility(View.VISIBLE);
            viewHolder.invoice_btn.setVisibility(View.GONE);
        } else {
            viewHolder.trackOrderBtn.setVisibility(View.GONE);
            viewHolder.invoice_btn.setVisibility(View.VISIBLE);
        }
        String amountStr = order.getAmount() + context.getResources().getString(R.string.euro);
        viewHolder.totalAmount.setText(amountStr);

        viewHolder.trackOrderBtn.setOnClickListener(v -> {
            onOrderItemClickListener.onItemClick(order);
        });

        viewHolder.invoice_btn.setOnClickListener(v -> {
            onOrderItemClickListener.onInvoiceClick(order);
        });
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return mOrders.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.text_items)
        TextView textItems;
        @BindView(R.id.items_description)
        TextView itemsDescription;
        @BindView(R.id.restaurant_name)
        TextView restaurantName;
        @BindView(R.id.restaurant_location)
        TextView restaurantLocation;
        @BindView(R.id.delivery_location)
        TextView deliveryLocation;
        @BindView(R.id.order_status)
        TextView orderStatus;
        @BindView(R.id.track_order_btn)
        TextView trackOrderBtn;
        @BindView(R.id.date_text)
        TextView dateText;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.total_amount)
        TextView totalAmount;
        @BindView(R.id.invoice_btn)
        TextView invoice_btn;
        @BindView(R.id.restaurant_image)
        ImageView restaurantImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

