package com.bydrone.droneapplication.Networking;

import com.bydrone.droneapplication.Models.CartResponse;
import com.bydrone.droneapplication.Models.Order;
import com.bydrone.droneapplication.Models.Restaurant;
import com.bydrone.droneapplication.Models.UserProfile;
import com.bydrone.droneapplication.Utils.Constants;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Path;

/**
 * @author Ankur Khandelwal on 02/04/18.
 */

public interface ApiClientInterface {

    @POST(Constants.REGISTER)
    Single<Response<ResponseBody>> register(@Body HashMap<String,String> userMap);

    @FormUrlEncoded
    @POST(Constants.LOGIN)
    Single<Response<ResponseBody>> login(@Field("grant_type") String grant_type, @Field("client_id") String client_id,
                             @Field("username") String username, @Field("password") String password);

    @GET(Constants.RESTAURANTS_SEARCH)
    Single<List<Restaurant>> restaurantSearchOnLocation(@Header("Authorization") String token, @Query("serviceable_point") String locationId);

    @GET(Constants.ORDERS_HISTORY)
    Single<List<Order>> getOrders(@Header("Authorization") String token);

    @GET(Constants.ORDERS_HISTORY+"{id}/")
    Single<Order> getOrderDetail(@Header("Authorization") String token, @Path(value = "id", encoded = true) String orderId);

    @POST(Constants.ORDER)
    Single<Order> order(@Header("Authorization") String token, @Body JsonObject orderId);

    @POST(Constants.CART)
    Single<CartResponse> cartReview(@Header("Authorization") String token, @Body JsonObject cartData);

    @POST(Constants.ORDERS_HISTORY)
    Call<ResponseBody> pickup(@Header("Authorization") String token, @Body JsonObject pickupdata);

    @GET(Constants.ORDER_STATUS+"{id}/")
    Call<ResponseBody> orderStatus(@Header("Authorization") String token, @Path(value = "id", encoded = true) String orderId);

    @FormUrlEncoded
    @PATCH(Constants.ORDER_STATUS+"{id}/")
    Call<ResponseBody> orderUpdateStatus(@Header("Authorization") String token, @Path(value = "id", encoded = true) String orderId, @Field("status") int statusID);

    @GET(Constants.SERVICABLE_POINTS)
    Call<ResponseBody> getLandingLocations(@Header("Authorization") String token, @Query("lat") double lat, @Query("lng") double lng);

    @GET(Constants.FOOD_ITEMS+"{id}/")
    Call<ResponseBody> foodMenu(@Header("Authorization") String token, @Path(value = "id", encoded = true) String restaurantId);


    @POST(Constants.REGISTER_DEVICE_FCM)
    Call<ResponseBody> registerDeviceFCM(@Header("Authorization") String token,@Body JsonObject deviceData);

    @GET(Constants.GET_SERVER_PAYMENT_TOKEN)
    Call<ResponseBody> getPaymentToken(@Header("Authorization") String token);

    @POST(Constants.SEND_PAYMENT_NONCE)
    Call<ResponseBody> sendPaymentNonce(@Header("Authorization") String token,@Body JSONObject paymentData);


    @GET(Constants.GET_LIVE_DRONE_DATA)
    Call<ResponseBody> getLiveDroneData(@Header("Authorization") String token);

    @GET(Constants.PROFILE)
    Single<UserProfile> getProfile(@Header("Authorization") String token);

    @GET(Constants.MODERATOR_DATA)
    Single<Response<ResponseBody>> getModeratorStatus(@Header("Authorization") String token);


    @Multipart
    @PATCH(Constants.PROFILE)
    Single<UserProfile> postProfile(@Header("Authorization") String token,
                                               @Part("first_name") RequestBody firstName,
                                               @Part("last_name") RequestBody lastName,
                                               @Part("email") RequestBody email,
                                               @Part("phone_number") RequestBody phoneNumber,
                                               @Part MultipartBody.Part image);

    @POST(Constants.FORGOT_PASSWORD)
    Single<Response<ResponseBody>> forgotPassword(@Body HashMap<String,String> userMap);

}

