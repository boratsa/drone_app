package com.bydrone.droneapplication.Cart;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.dropin.utils.PaymentMethodType;
import com.braintreepayments.api.interfaces.BraintreeErrorListener;
import com.braintreepayments.api.models.GooglePaymentRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.braintreepayments.cardform.view.CardForm;
import com.bumptech.glide.Glide;
import com.bydrone.droneapplication.Adapter.CartItemAdapter;
import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.Interface.OnItemUpdateListener;
import com.bydrone.droneapplication.Models.CartItem;
import com.bydrone.droneapplication.Models.CartResponse;
import com.bydrone.droneapplication.Models.FoodItem;
import com.bydrone.droneapplication.Models.LandingLocation;
import com.bydrone.droneapplication.Models.Order;
import com.bydrone.droneapplication.Models.Restaurant;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.LogUtils;
import com.bydrone.droneapplication.Utils.StringConstants;
import com.bydrone.droneapplication.Utils.Utility;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.WalletConstants;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.bydrone.droneapplication.Utils.Utility.getDecimalValue;

public class CartFragment extends Fragment implements OnItemUpdateListener , BraintreeErrorListener {

    private static final String TAG = "CartFragment";
    private ArrayList<FoodItem> foodItemsList = new ArrayList<>();
    private CartItemAdapter adapter;
    private Button viewCart;
    private String selectedLocationStr = "";
    private String selectedRestaurantStr = "";
    private LandingLocation selectedLandingLocation;
    private Restaurant selectedRestaurant;
    private Context context;
    private AppCompatActivity activity;
    private int cartId = -1;
    private int REQUEST_CODE = 101;
    private InputMethodManager inputMethodManager;
    @BindView(R.id.delivery_location)
    TextView delivery_location;
    @BindView(R.id.totalCartAmount)
    TextView totalCartAmount;
    @BindView(R.id.totalTaxAmount)
    TextView totalTaxAmount;
    @BindView(R.id.finalAmount)
    TextView finalAmount;
    @BindView(R.id.totalWeight)
    TextView totalWeight;
    @BindView(R.id.shippingCharge)
    TextView shippingCharge;
    @BindView(R.id.place_order)
    Button placeOrder;
    @BindView(R.id.restaurant_name)
    TextView restaurant_name;
    @BindView(R.id.back_button)
    ImageButton back_button;
    @BindView(R.id.cart_layout_scrollview)
    NestedScrollView cartLayout;
    @BindView(R.id.no_cart_layout)
    RelativeLayout noCartLayout;
    @BindView(R.id.clear_all_text)
    TextView clearAllText;
    @BindView(R.id.cartReviewItems)
    RecyclerView rvFoodItems;
    @BindView(R.id.payment_add_button)
    RelativeLayout paymentAddButton;
    @BindView(R.id.paymentTypeImage)
    ImageView paymentTypeImage;
    @BindView(R.id.paymentModeDesc)
    TextView paymentModeDesc;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.restaurant_image)
    ImageView restaurantImage;
    @BindView(R.id.suggestionsEditText)
    TextInputEditText suggestionsEditText;
    @BindView(R.id.cart_coordinate)
    CoordinatorLayout cartCoordinate;
    @BindView(R.id.payment_card)
    CardView paymentCardView;
    @BindView(R.id.snackbar_layout)
    LinearLayout noOrderSnackbarLayout;
    @BindView(R.id.snackbar_text)
    TextView noOrderSnackbarText;

    private OnItemUpdateListener onItemUpdateListener;
    private Boolean shouldProceedOrder = false;
    private Boolean isPaymentDone = false;
    private String clientToken = "sandbox_24q58q8x_jcjycqcpthfgkckr";
    private PaymentMethodNonce paymentMethodNonce = null;
    private CompositeDisposable compositeDisposable;
    private View rootView;
    private String finalTotal = "0.00";
    private String specialInstruction = "";
    @Inject
    public SharedPreferences preferences;

    @Inject
    public ApiClientInterface apiClientInterface;

    @Inject
    public CartSingleton cartSingleton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_cart,container, false);
        ButterKnife.bind(this,rootView);
        updateUI();
        return rootView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        onItemUpdateListener = this;
        compositeDisposable = new CompositeDisposable();
    }

    private void updateUI() {
        context = getContext();
        activity = ((AppCompatActivity) getActivity());
        inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        JsonArray cartItems = cartSingleton.getFormattedCartItems();
        LogUtils.printLog(TAG, "cartItems = " + cartItems);
        if (cartSingleton.getCart().isEmpty()) {
            toggleCartLayout(false);
        } else {
            toggleCartLayout(true);
            setLocationAndRestaurant();
            sendCartItemstoServer(cartItems);
            getClientTokenFromServer();
        }
        setupOrderView();
        updateCartUi();
    }



    private void getClientTokenFromServer() {
        try {
            Call<ResponseBody> serverTokenResponse = apiClientInterface.getPaymentToken("Bearer " + Utility.getJwtToken());
            serverTokenResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.code() == 200 || response.code() == 201) {
                            if (response.body() != null) {
                                JSONObject object = new JSONObject(response.body().string());
                                String token = object.getString("client_token");
                                LogUtils.printLog(TAG, "token =" + token);
                                clientToken = token;
                            }
                        } else {

                            if (response.errorBody() != null) {
                                JSONObject object = new JSONObject(response.errorBody().string());
                                String message = object.getString("error");
                                Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateCartUi() {
        if (isPaymentDone) {
            shouldProceedOrder = true;
            placeOrder.setText(context.getResources().getString(R.string.place_order));
        } else {
            placeOrder.setText(context.getResources().getString(R.string.proceed_to_payment));
        }
    }

    private void setLocationAndRestaurant() {
        selectedLandingLocation = cartSingleton.getSelectedLandingLocation();
        selectedLocationStr = selectedLandingLocation.getAddress();
        selectedRestaurant = cartSingleton.getSelectedRestaurant();
        selectedRestaurantStr = selectedRestaurant.getName() == null ? " " : selectedRestaurant.getName();
        delivery_location.setText(selectedLandingLocation.getAddress());
        delivery_location.setTypeface(null, Typeface.BOLD);
        restaurant_name.setTypeface(null, Typeface.BOLD);
        restaurant_name.setText(cartSingleton.getRestaurantName());

        Glide.with(context).load(selectedRestaurant.getImage().getThumbnail()).into(restaurantImage);
    }

    private void sendCartItemstoServer(JsonArray cartItems) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            foodItemsList = new ArrayList<>();
            JsonObject cartObj = new JsonObject();
            cartObj.addProperty("serviceable_point", selectedLandingLocation.getId());
            cartObj.add("cart_items", cartItems);

            Single<CartResponse> cartObservable = apiClientInterface.cartReview("Bearer " + Utility.getJwtToken(), cartObj);
            cartObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(getCartResponseObserver());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private SingleObserver<? super CartResponse> getCartResponseObserver() {
        return new SingleObserver<CartResponse>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(CartResponse cartResponse) {
                progressBar.setVisibility(View.GONE);
                for (int i = 0; i < cartResponse.getCartItems().size(); i++) {
                    CartItem item = cartResponse.getCartItems().get(i);
                    FoodItem foodItem = item.getFoodItem();
                    int quantity = item.getQuantity();
                    cartSingleton.getCart().updateFoodItem(foodItem, quantity);
                }
                cartId = cartResponse.getId();
                updatePrices(cartResponse);
                setupCartAdapter();
            }

            @Override
            public void onError(Throwable e) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(context.getApplicationContext(), getResources().getString(R.string.error_generic), Toast.LENGTH_LONG).show();
            }
        };
    }

    private void updatePrices(CartResponse response) {
        String price = response.getPrice() + getResources().getString(R.string.euro);
        String tax = response.getTax() + getResources().getString(R.string.euro);
        finalTotal = response.getTotalPrice();
        String finalTotalStr = finalTotal + getResources().getString(R.string.euro);
        Double cartTotalWeight = Double.valueOf(response.getWeight()) / 1000;
        String weightStr = getDecimalValue(cartTotalWeight) + getResources().getString(R.string.kg);
        String shipping = response.getShippingCharge() + getResources().getString(R.string.euro);
        totalCartAmount.setText(price);
        totalTaxAmount.setText(tax);
        finalAmount.setText(finalTotalStr);
        totalWeight.setText(weightStr);
        shippingCharge.setText(shipping);
    }

    private void setupCartAdapter() {
        adapter = new CartItemAdapter(cartSingleton.getCart().getFoodItems(), context, onItemUpdateListener,cartSingleton);
        rvFoodItems.setAdapter(adapter);
        rvFoodItems.setLayoutManager(new LinearLayoutManager(context.getApplicationContext()));
    }

    private void toggleCartLayout(Boolean shouldCartItemShow) {
        if (shouldCartItemShow) {
            cartLayout.setVisibility(View.VISIBLE);
            noCartLayout.setVisibility(View.GONE);
            placeOrder.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        } else {
            cartLayout.setVisibility(View.GONE);
            noCartLayout.setVisibility(View.VISIBLE);
            clearAllText.setVisibility(View.GONE);
            placeOrder.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.back_button)
    void goBack() {
        Navigation.findNavController(rootView).navigateUp();
    }

    @OnClick(R.id.place_order)
    void placeOrder() {
        if (!isPaymentDone) {
            startPaymentFragment();
        } else {
            if (shouldProceedOrder) {
                try {
                    progressBar.setVisibility(View.VISIBLE);
                    JsonObject cartObj = new JsonObject();
                    cartObj.addProperty("cart", cartId);
                    if(suggestionsEditText.getText().length()>0){
                        cartObj.addProperty("special_instruction",suggestionsEditText.getText().toString());
                    }
                    if (paymentMethodNonce != null) {
                        cartObj.addProperty("payment_method_nonce", paymentMethodNonce.getNonce());
                    }

                    Single<Order> orderObservable = apiClientInterface.order("Bearer " + Utility.getJwtToken(), cartObj);
                    orderObservable.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(getOrderObserver());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private SingleObserver<? super Order> getOrderObserver() {
        return new SingleObserver<Order>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(Order order) {
                progressBar.setVisibility(View.GONE);
                startFinishActivty(order);
            }

            @Override
            public void onError(Throwable e) {
                progressBar.setVisibility(View.GONE);
                if (e.getMessage().contains("402")) {
                    Toast.makeText(context.getApplicationContext(), getResources().getString(R.string.error_transaction_failed), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context.getApplicationContext(), getResources().getString(R.string.error_generic), Toast.LENGTH_LONG).show();
                }
            }
        };
    }

    private void setupOrderView() {
        if(!cartSingleton.isOrderAvailable()){
            noOrderSnackbarLayout.setVisibility(View.VISIBLE);
            noOrderSnackbarText.setText(cartSingleton.getOrderReasoning());

            placeOrder.setVisibility(View.GONE);
            paymentCardView.setVisibility(View.GONE);
        }else{
            noOrderSnackbarLayout.setVisibility(View.GONE);
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        compositeDisposable.clear();
    }

    private void startFinishActivty(Order order) {
        cartSingleton.getCart().clearCart();
        cartSingleton.setSelectedOrder(order);
        Bundle bundle = new Bundle();
        bundle.putInt(StringConstants.KEY_ORDER_ID,order.getId());
        Navigation.findNavController(rootView)
                .navigate(R.id.action_cart_to_finishFragment, bundle , null, null);
    }

    @OnClick(R.id.clear_all_text)
    public void clearCart() {
        cartSingleton.getCart().clearCart();
        updateUI();
    }

    @Override
    public void onItemUpdate() {
        LogUtils.printLog(TAG, "cart items =" + cartSingleton.getCartFoodItems().size());
        updateUI();
    }

    @Override
    public void onMaxWeightReached() {
        Snackbar snackbar = Snackbar
                .make(cartCoordinate, context.getResources().getString(R.string.overweight_cart_text), Snackbar.LENGTH_LONG);
        View view = snackbar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        view.setPadding(8,8,8,8);
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(getResources().getColor(R.color.white));
        snackbar.show();
    }

    @OnClick(R.id.payment_add_button)
    public void startBrainTreeFragment() {
        LogUtils.printLog(TAG, "starting braintree");
        startPaymentFragment();
    }

//    @OnClick(R.id.suggestion_card_layout)
    public void showBottomSheet(){
        suggestionsEditText.requestFocus();
        suggestionsEditText.setText(specialInstruction);

        inputMethodManager.showSoftInput(suggestionsEditText, InputMethodManager.SHOW_IMPLICIT);
    }

//    @OnClick(R.id.instruction_header)
    public void dissmissBottomSheet(){
        hideKeyboard();
    }

    private void hideKeyboard() {
        View v = activity.getWindow().getCurrentFocus();
        if (v != null) {
            inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        LogUtils.printLog(TAG,"onResume fragment");
    }

//    @OnClick(R.id.add_instruction_btn)
    public void addInstructionToView(){
        dissmissBottomSheet();
        specialInstruction = suggestionsEditText.getText().toString();
    }


    void startPaymentFragment() {
        if (clientToken.equals("")) {
            Toast.makeText(context.getApplicationContext(), getResources().getString(R.string.payment_token_error), Toast.LENGTH_LONG).show();
        } else {
            LogUtils.printLog(TAG, "clientToken = " + clientToken);
            DropInRequest dropInRequest = new DropInRequest().clientToken(clientToken);
            dropInRequest.vaultManager(true);
            dropInRequest.cardholderNameStatus(CardForm.FIELD_OPTIONAL);
            enableGooglePay(dropInRequest);
            startActivityForResult(dropInRequest.getIntent(context), REQUEST_CODE);
        }
    }

    @OnClick(R.id.suggestionsEditText)
    void scrollWithKeyboard(){
        cartLayout.fullScroll(View.FOCUS_DOWN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                // use the result to update your UI and send the payment method nonce to your server
                LogUtils.printLog(TAG, "Payment success = " + result.getPaymentMethodNonce() + " type=  " + result.getPaymentMethodType());
                PaymentMethodNonce paymentMethodNonce = result.getPaymentMethodNonce();
                String content = paymentMethodNonce.getDescription();
                LogUtils.printLog(TAG, "payment nounce =" + content);
                String mNonce = paymentMethodNonce.getNonce();
                LogUtils.printLog(TAG, "nonce = " + mNonce);
                updatePaymentUI(result.getPaymentMethodType(), content, paymentMethodNonce);
            } else if (resultCode == RESULT_CANCELED) {
                // the user canceled
                LogUtils.printLog(TAG, "user cancelled");
            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                LogUtils.printLog(TAG, "error occured ");
            }
        }
    }

    private void updatePaymentUI(PaymentMethodType paymentMethodType, String paymentMethodNonceString, PaymentMethodNonce methodNonce) {

        if (paymentMethodType == PaymentMethodType.PAYPAL) {
            paymentTypeImage.setBackground(getResources().getDrawable(R.drawable.ic_paypal));
        } else {
            paymentTypeImage.setBackground(getResources().getDrawable(R.drawable.ic_visa));
        }
        paymentModeDesc.setText(paymentMethodNonceString);
        isPaymentDone = true;
        paymentMethodNonce = methodNonce;
        updateCartUi();
    }

    private void enableGooglePay(DropInRequest dropInRequest) {
        GooglePaymentRequest googlePaymentRequest = new GooglePaymentRequest()
                .transactionInfo(TransactionInfo.newBuilder()
                        .setTotalPrice(finalTotal)
                        .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
                        .setCurrencyCode("Euro")
                        .build())
                .billingAddressRequired(true);
        // Optional in sandbox; if set in sandbox, this value must be
        // a valid production Google Merchant ID.
//      .googleMerchantId("merchant-id-from-google");

        dropInRequest.googlePaymentRequest(googlePaymentRequest);
    }

    private void sendNonceToServer(String paymentMethodNonce) {
        try {
            progressBar.setVisibility(View.VISIBLE);
            JSONObject object = new JSONObject();
            object.put("payment_method_nonce", paymentMethodNonce);
            Call<ResponseBody> serverTokenResponse = apiClientInterface.sendPaymentNonce("Bearer " + Utility.getJwtToken(), object);
            serverTokenResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        if (response.code() == 200 || response.code() == 201) {
                            if (response.body() != null) {
                                LogUtils.printLog(TAG, "Send to server success");
                            }
                        } else {

                            if (response.errorBody() != null) {
                                JSONObject object = new JSONObject(response.errorBody().string());
                                String message = object.getString("error");
                                Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(Exception error) {
        LogUtils.printLog(TAG,"BrainTree "+ error.getMessage());
    }
}
