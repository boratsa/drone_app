package com.bydrone.droneapplication.Cart;

import android.content.Context;

import com.bydrone.droneapplication.BuildConfig;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.bydrone.droneapplication.Utils.Constants;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class CartModule {

    @Provides
    @Singleton
    CartSingleton provideCartSingletonInstance(Context context) {
        return new CartSingleton(context);
    }
}
