package com.bydrone.droneapplication.Cart;

import android.content.Context;

import com.bydrone.droneapplication.Models.Cart;
import com.bydrone.droneapplication.Models.Category;
import com.bydrone.droneapplication.Models.FoodItem;
import com.bydrone.droneapplication.Models.LandingLocation;
import com.bydrone.droneapplication.Models.Order;
import com.bydrone.droneapplication.Models.Restaurant;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;


public class CartSingleton {
    private String deliveryAddress;
    private JsonArray formattedCartItems = new JsonArray();
    private Restaurant selectedRestaurant = null;
    private LandingLocation selectedLandingLocation = null;
    private Order selectedOrder = null;
    private Cart cart = new Cart();
    private final Context context;
    private boolean isOrderAvailable = true;

    public boolean isOrderAvailable() {
        return isOrderAvailable;
    }

    public void setOrderAvailable(boolean orderAvailable) {
        isOrderAvailable = orderAvailable;
    }

    public String getOrderReasoning() {
        return orderReasoning;
    }

    public void setOrderReasoning(String orderReasoning) {
        this.orderReasoning = orderReasoning;
    }

    private String orderReasoning = "";
    private ArrayList<Category> categories = new ArrayList<>();

    public CartSingleton(Context context) {
        this.context = context;
    }

    public LandingLocation getSelectedLandingLocation() {
        return selectedLandingLocation;
    }
    public Restaurant getSelectedRestaurant() {
        return selectedRestaurant;
    }

    public void setSelectedRestaurant(Restaurant selectedRestaurant) {
        this.selectedRestaurant = selectedRestaurant;
    }

    public String getRestaurantName() {
        return this.selectedRestaurant.getName();
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }

    public boolean isRestaurantChanged(Restaurant currentRestaurant) {
        if (this.selectedRestaurant.getId() == 0) {
            this.selectedRestaurant = currentRestaurant;
            return false;
        } else if (this.selectedRestaurant.getId() == currentRestaurant.getId()) {
            return false;
        } else {
            return true;
        }
    }

    public void clearCartAndSetRestuarantId(Restaurant restaurant) {
        clearCart();
        this.setSelectedRestaurant(restaurant);
    }

    public void revertSelectedRestaurant(){
        this.setSelectedRestaurant(this.getCart().getFoodItems().get(0).getRestaurant());
    }

    public void clearCart() {
        this.getCart().clearCart();
    }


    public void setFoodItems(FoodItem foodItem,int itemCount) {
        this.cart.updateFoodItem(foodItem,itemCount);
    }

    public ArrayList<FoodItem> getCartFoodItems(){
        return this.cart.getFoodItems();
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public JsonArray getFormattedCartItems() {
        this.formattedCartItems = new JsonArray();
        ArrayList<FoodItem> items = this.getCart().getFoodItems();
        JsonObject cartData = null;
        for (int i = 0; i < items.size(); i++) {
            try {
                cartData = new JsonObject();
                cartData.addProperty("menu_item", items.get(i).getId());
                cartData.addProperty("quantity", items.get(i).getQuantity());
                formattedCartItems.add(cartData);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return formattedCartItems;
    }

    public void setSelectedLandingLocation(LandingLocation selectedLandingLocation) {
        this.selectedLandingLocation = selectedLandingLocation;
    }

    public LandingLocation getDefaultLandingLocation() {
        LandingLocation location = new LandingLocation(10,60.189216,24.83045,"Area 51, Otakaari 5", "176.5141408 m");
        this.selectedLandingLocation = location;
        return this.selectedLandingLocation;
    }

    public Order getSelectedOrder() {
        return selectedOrder;
    }

    public void setSelectedOrder(Order selectedOrder) {
        this.selectedOrder = selectedOrder;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public String getOrderStatusString(int id){
        switch (id){
            case -1:
                return "Order Cancelled";
            case 0:
                return "Order Created";
            case 1:
                return "Order Approved";
            case 2:
                return "Assigned Drone";
            case 3:
                return "Drone arrived at store";
            case 4:
                return "Items attached to drone";
            case 5:
                return "Departure from store";
            case 6:
                return "Arrival to Destination";
            case 7:
                return "Received at Destination";
            default:
                return "Order Delivered";
        }
    }


    public String getStatusDescriptionText(int id){
        switch (id){
            case 0:
                return "Thank you! Awaiting confirmation.";
            case 1:
                return "Yipee! Restaurant is preparing your order within 15-20min";
            case 2:
                return "Our Drone has been assigned to you";
            case 3:
                return "Our Drone has reached at restaurant.";
            case 4:
                return "This is the fun part! Your order is being loaded and ready to fly!";
            case 5:
                return "We are up and away! Your delivery will arrive in 5 minutes! Please be ready and at the delivery site - we cant wait";
            case 6:
                return "Please swipe after collecting your package. Please stand back 5m from drone, Your drone is about to take-off";
            case 7:
                return "Yipee! Delivered.";
            default:
                return "Order Completed :).";
        }
    }
}
