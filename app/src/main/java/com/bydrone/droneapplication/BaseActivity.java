package com.bydrone.droneapplication;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.google.android.material.snackbar.Snackbar;

import java.text.DecimalFormat;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;
import dagger.android.AndroidInjection;

public class BaseActivity extends AppCompatActivity {
    @Inject
    public SharedPreferences preferences;

    @Inject
    public ApiClientInterface apiClientInterface;

    @Inject
    public CartSingleton cartSingleton;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
    }

    public void showSnackBar(View coordinateView,String s, String actionString, int snackbarDuration){
        Snackbar snackbar = Snackbar.make(coordinateView, s, snackbarDuration);
        View view = snackbar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        final CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
        params.setMargins(16, 16, 16, 16);

        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(getResources().getColor(R.color.white));
        snackbar.show();
    }

    public String getDecimalValue(Double data){
        DecimalFormat df = new DecimalFormat("####0.00");
        return df.format(data);
    }
}
