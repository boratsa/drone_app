package com.bydrone.droneapplication.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;

import butterknife.OnClick;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bydrone.droneapplication.Adapter.FoodItemsAdapter;
import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.Interface.OnItemUpdateListener;
import com.bydrone.droneapplication.Models.Category;
import com.bydrone.droneapplication.Models.FoodItem;
import com.bydrone.droneapplication.Models.LandingLocation;
import com.bydrone.droneapplication.Models.Restaurant;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.LogUtils;
import com.bydrone.droneapplication.Utils.Utility;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Transition;
import androidx.transition.TransitionInflater;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.AndroidSupportInjection;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import javax.inject.Inject;

import static com.bydrone.droneapplication.Utils.Utility.getDecimalValue;

public class FoodItemFragment extends Fragment implements OnItemUpdateListener, TabLayout.OnTabSelectedListener {
    private static final String TAG = "FoodItemFragment";
    private ArrayList<FoodItem> foodItemsList = new ArrayList<>();
    private ArrayList<Category> categories = new ArrayList<>();
    private FoodItemsAdapter foodItemAdapter;
    private String selectedLocationId = "";
    private String restaurantName = "";
    private int restaurantID = -1;
    private AppCompatActivity activity;
    private OnItemUpdateListener onItemUpdateListener;
    private String selectedLocationStr = "";
    private String selectedRestaurantStr = "";
    private LandingLocation selectedLandingLocation;
    private Restaurant selectedRestaurant;
    private LinearLayoutManager linearLayoutManager;
    private View rootView;
    private Context context;
    private Transition sharedElementEnterTransition;
    private String vendorEmail = "";
    private String vendorContact = "";
    @BindView(R.id.foodItemsRecyclerView)
    RecyclerView recyclerViewFoodItems;
    @BindView(R.id.restaurant_image)
    ImageView restaurantImage;
    @BindView(R.id.toolbar_food_item)
    Toolbar toolbarFoodItem;
    @BindView(R.id.collapse_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    //    @BindView(R.id.restaurant_name)
//    TextView restaurantNameTextView;
    @BindView(R.id.cart_view_fill)
    RelativeLayout cartViewFill;
    @BindView(R.id.app_bar_food_item)
    AppBarLayout appBarLayout;
    @BindView(R.id.cart_total)
    TextView cartTotal;
    @BindView(R.id.cart_weight)
    TextView cartWeight;
    @BindView(R.id.categoryTabLayout)
    TabLayout categoryTabLayout;
    @BindView(R.id.coordinate_food_item)
    CoordinatorLayout coordinateFoodItem;
    @Inject
    public SharedPreferences preferences;

    @Inject
    public ApiClientInterface apiClientInterface;

    @Inject
    public CartSingleton cartSingleton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_food_items,container, false);
        ButterKnife.bind(this,rootView);
        LogUtils.printLog(TAG,"onCreateView");
        setupUI();
        return rootView;
    }

    /**
     * First method to call after activity onCreate()
     * @param context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
        LogUtils.printLog(TAG,"onAttach");
    }

    /**
     * Next method from onCreate() of activity, then onCreateview()
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedElementEnterTransition = TransitionInflater.from(context)
                .inflateTransition(android.R.transition.move);
        Transition sharedElementReturnTransition = TransitionInflater.from(context)
                .inflateTransition(android.R.transition.move).setDuration(2000);
        setSharedElementReturnTransition(sharedElementReturnTransition);
        onItemUpdateListener = this;
        LogUtils.printLog(TAG,"onCreate");

    }

    private void setupUI() {
        activity.setSupportActionBar(toolbarFoodItem);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);


        selectedRestaurant = cartSingleton.getSelectedRestaurant();
        if (selectedRestaurant != null) {
            selectedRestaurantStr = selectedRestaurant.getName();
            restaurantImage.setTransitionName(selectedRestaurant.getName());
            Glide.with(this).load(selectedRestaurant.getImage().getFullSize())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            setStatusBarColor(resource);
                            return false;
                        }
                    })
                    .into(restaurantImage);
            collapsingToolbarLayout.setTitle(selectedRestaurantStr);
        }

        linearLayoutManager = new LinearLayoutManager(activity);

        toolbarFoodItem.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
        toolbarFoodItem.setNavigationOnClickListener(v -> Navigation.findNavController(rootView).navigateUp());
        toolbarFoodItem.setTitle(" ");
        toolbarFoodItem.setTitleTextColor(getResources().getColor(R.color.white));
        toolbarFoodItem.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));

        collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.black));
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = true;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        toolbarFoodItem.getNavigationIcon().setTint(getResources().getColor(R.color.black));
                    } else {
                        toolbarFoodItem.setNavigationIcon(getResources().getDrawable(R.drawable.ic_back));
                    }
                    isShow = true;
                    categoryTabLayout.setVisibility(View.VISIBLE);
                } else if (isShow) {
                    toolbarFoodItem.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        toolbarFoodItem.getNavigationIcon().setTint(getResources().getColor(R.color.white));
                    }
                    isShow = false;
                    categoryTabLayout.setVisibility(View.GONE);
                }
            }
        });

        if (selectedRestaurant != null && foodItemsList.size()==0) {
            fetchAllFoodItems(selectedRestaurant.getId());
        }else{
            setUpRecyclerView(foodItemsList,categories,vendorEmail,vendorContact);
        }

        setCartView();

        recyclerViewFoodItems.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                int position = linearLayoutManager.findFirstVisibleItemPosition();
                LogUtils.printLog(TAG, "Visible = " + position);
                int headerPosition = foodItemAdapter.getHeaderPositionForItem(position);
                LogUtils.printLog(TAG, "HeaderPos = " + headerPosition);
                int categoryIdForHeader = foodItemAdapter.getCategoryItemId(headerPosition);
                int catIndex = getCategoryIndexFromId(categoryIdForHeader);

                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        if (catIndex != -1) {
                            selectTabOnScroll(catIndex);
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        if (catIndex != -1) {
                            selectTabOnScroll(catIndex);
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        break;

                }
            }
        });
    }

    private void setStatusBarColor(Drawable resource) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = activity.getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.TRANSPARENT);
            }
    }

    @OnClick(R.id.cart_view_fill)
    void goToCart() {
//        Intent intent = new Intent(FoodItemsActivity.this, CartActivity.class);
//        startActivity(intent);
        Navigation.findNavController(rootView).navigate(R.id.action_fooditem_to_cartFragment);
        activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    private void selectTabOnScroll(int categoryIndex) {
//        categoryTabLayout.getTabAt(catIndex).select()
        categoryTabLayout.removeOnTabSelectedListener(this);
        categoryTabLayout.getTabAt(categoryIndex).select();
        categoryTabLayout.addOnTabSelectedListener(this);
    }

    private int getCategoryIndexFromId(int id) {
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getId() == id) {
                return i;
            }
        }
        return -1;
    }

    private void setUpRecyclerView(ArrayList<FoodItem> foodItemsList, ArrayList<Category> categories,String vendorEmail,String vendorContact) {
        cartSingleton.setCategories(categories);
        foodItemAdapter = new FoodItemsAdapter(foodItemsList, context, onItemUpdateListener, categories,cartSingleton,vendorEmail,vendorContact);
        recyclerViewFoodItems.setAdapter(foodItemAdapter);
        recyclerViewFoodItems.setLayoutManager(linearLayoutManager);
//        recyclerViewFoodItems.addItemDecoration(new StickHeaderItemDecoration(foodItemAdapter));

        addTabs();
    }

    private void addTabs() {
        for (Category category : categories) {
            TabLayout.Tab tab = categoryTabLayout.newTab();
            tab.setTag(category.getId());
            tab.setText(category.getName());
            categoryTabLayout.addTab(tab);
        }

        categoryTabLayout.addOnTabSelectedListener(this);

    }


    @Override
    public void onResume() {
        super.onResume();
        LogUtils.printLog(TAG, "onResume");
        updateCartData();
    }

    private void updateCartData() {
        if (foodItemAdapter != null) {
            foodItemAdapter.updateItemCount();
            setCartView();
        }

    }

    private void setCartView() {
        if (cartSingleton.getCart().isEmpty()) {
            Utility.slideToBottom(cartViewFill);
//            cartViewFill.setVisibility(View.GONE);
        } else {
//            cartViewFill.setVisibility(View.VISIBLE);
            double remainingWeight = cartSingleton.getCart().getRemainingCartWeight();
            String weightStr = getDecimalValue(remainingWeight) + getResources().getString(R.string.kg);
            double price = cartSingleton.getCart().getCartTotal();
            String priceStr = getDecimalValue(price) + getResources().getString(R.string.euro);
            cartTotal.setText(priceStr);
            cartWeight.setText(weightStr);
            if(cartViewFill.getVisibility() == View.GONE) {
                Utility.slideToTop(cartViewFill);
            }
        }
        setupRecyclerViewHeight();
    }

    private void setupRecyclerViewHeight() {
        if(!cartSingleton.getCart().isEmpty()){
            float paddingDp = context.getResources().getDimension(R.dimen.size50);
            float density = context.getResources().getDisplayMetrics().density;
            int paddingPixel = (int)(paddingDp * density);
            recyclerViewFoodItems.setPadding(0,0,0,(int)paddingDp);
        }else{
            recyclerViewFoodItems.setPadding(0,0,0,0);
        }
    }

    private void fetchAllFoodItems(int restaurantID) {
        try {
            Call<ResponseBody> allfoodItemsResponse = apiClientInterface.foodMenu("Bearer " + Utility.getJwtToken(), String.valueOf(restaurantID));
            allfoodItemsResponse.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        if (response.code() == 200 || response.code() == 201) {
                            if (response.body() != null) {
                                JSONObject object = new JSONObject(response.body().string());
                                JSONArray catArray = object.getJSONArray("categories");
                                Category category;
                                for (int i = 0; i < catArray.length(); i++) {
                                    JSONObject catObj = catArray.getJSONObject(i);
                                    int catId = catObj.getInt("id");
                                    String catName = catObj.getString("name");
                                    JSONArray array = catObj.getJSONArray("menu_items");
                                    ArrayList<FoodItem> foodItemArrayList = new ArrayList<>();
                                    FoodItem foodItems;
                                    for (int j = 0; j < array.length(); j++) {
                                        JSONObject jo = array.getJSONObject(j);

                                        JSONObject imageObject = jo.getJSONObject("image");
                                        String imageUrlSmall = imageObject.getString("small_square_crop");
                                        String imageUrlFull = imageObject.getString("full_size");
                                        String imageThumbnail = imageObject.getString("thumbnail");
                                        String imageUrlMedium = imageObject.getString("medium_square_crop");
                                        String name = jo.getString("name");
                                        String weight = jo.getString("weight");
                                        String price = jo.getString("price");
                                        int id = jo.getInt("id");
                                        String foodType = jo.getString("food_type");
                                        boolean isAvailable = jo.getBoolean("is_available");
                                        if(isAvailable) {
                                            foodItems = new FoodItem(name, weight, price, id, selectedRestaurant, foodType,
                                                    imageUrlSmall, imageUrlMedium, imageUrlFull, imageThumbnail, isAvailable);
                                            foodItemArrayList.add(foodItems);
                                        }
                                    }
                                    category = new Category(catId, catName, foodItemArrayList);
                                    foodItemsList.addAll(foodItemArrayList);
                                    categories.add(category);
                                }

                                JSONObject vendorObject = object.getJSONObject("owner");
                                vendorEmail  = vendorObject.getString("email");
                                vendorContact = vendorObject.getString("phone_number");

                                setUpRecyclerView(foodItemsList, categories,vendorEmail,vendorContact);
                            }
                        } else {
                            if (response.errorBody() != null) {
                                JSONObject object = new JSONObject(response.errorBody().string());
                                String message = object.getString("error");
                                Toast.makeText(context.getApplicationContext(), message, Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    //progressBar.setVisibility(View.GONE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemUpdate() {
        updateCartData();
    }

    @Override
    public void onMaxWeightReached() {
        String overweightText = context.getResources().getString(R.string.overweight_cart_text)
                .concat(String.valueOf(cartSingleton.getCart().getMaxAllowedCartWeight()));
        Snackbar snackbar = Snackbar
                .make(coordinateFoodItem, overweightText, Snackbar.LENGTH_LONG);
        View view = snackbar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        view.setPadding(8,8,8,8);
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(getResources().getColor(R.color.white));
        snackbar.show();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int tabId = (int) tab.getTag();
        LogUtils.printLog(TAG, "tabId = " + tabId);
        int position = foodItemAdapter.getCategoryItemPosition(tabId);
        if (position != -1)
            linearLayoutManager.scrollToPositionWithOffset(position, 0);
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
}
