package com.bydrone.droneapplication.Fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.LogUtils;
import com.bydrone.droneapplication.Utils.StringConstants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;

import static com.bydrone.droneapplication.Utils.Constants.CALL_PHONE_PERMISSION;
import static com.bydrone.droneapplication.Utils.Constants.WRITE_EXTERNAL_STORAGE;

/**
 * @author Ankur Khandelwal on 2019-08-23.
 */
public class HelpFragment extends Fragment {
    private String TAG = "HelpFragment";
    private View rootView;
    private Context context;
    private AppCompatActivity activity;
    private int REQUEST_PERMISSIONS_REQUEST_CODE = 0x4;

    @BindView(R.id.toolbar_help)
    Toolbar toolbarHelp;
    @BindView(R.id.email_layout)
    RelativeLayout emailLayout;
    @BindView(R.id.call_layout)
    RelativeLayout callLayout;
    @BindView(R.id.support_contact_number)
    TextView supportContactNumber;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_help,container, false);
        ButterKnife.bind(this,rootView);
        init();
        return rootView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }

    private void init() {
        toolbarHelp.setNavigationOnClickListener(v -> Navigation.findNavController(rootView).navigateUp());
        toolbarHelp.setTitle(getResources().getString(R.string.help));
        toolbarHelp.setTitleTextColor(getResources().getColor(R.color.black));
        toolbarHelp.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black));

        supportContactNumber.setText(context.getResources().getString(R.string.default_contact_number));
    }

    @OnClick(R.id.email_layout)
    void sendEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto",context.getResources().getString(R.string.default_email),null));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Help -");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    @OnClick(R.id.call_layout)
    void makePhoneCall(){
        if(checkPermission()) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + context.getResources().getString(R.string.default_contact_number)));
            startActivity(intent);
        }else{
            requestPermission();
        }
    }


    private void requestPermission() {
        Log.i(TAG, "Displaying permission rationale to provide additional context.");
        requestPermissions(new String[]{CALL_PHONE_PERMISSION}, REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private boolean checkPermission() {
        int permissionState = context.checkCallingOrSelfPermission(CALL_PHONE_PERMISSION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Permission granted
                makePhoneCall();
            } else {
                LogUtils.printLog(TAG, "Permission result -> Not granted");
                //permission not granted.
            }
        }
    }

}
