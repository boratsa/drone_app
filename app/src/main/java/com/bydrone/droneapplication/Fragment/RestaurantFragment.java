package com.bydrone.droneapplication.Fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.FragmentNavigator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.bydrone.droneapplication.Adapter.RestaurantAdapter;
import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.Interface.OnRestaurantClickListener;
import com.bydrone.droneapplication.Models.LandingLocation;
import com.bydrone.droneapplication.Models.Restaurant;
import com.bydrone.droneapplication.Models.UserProfile;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Splash.SplashActivity;
import com.bydrone.droneapplication.Utils.Constants;
import com.bydrone.droneapplication.Utils.LogUtils;
import com.bydrone.droneapplication.Utils.StringConstants;
import com.bydrone.droneapplication.Utils.Utility;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.bydrone.droneapplication.Utils.Utility.getDecimalValue;

/**
 * @author Ankur Khandelwal on 2019-07-23.
 */
public class RestaurantFragment extends Fragment implements OnRestaurantClickListener, NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "RestaurantFragment";
    private ArrayList<Restaurant> restaurantsList;
    private RestaurantAdapter adapter;
    private Context context;
    private AppCompatActivity activity;
    private String defaultSelectedLocId = "7";
    private TextView header_userEmail;
    private View headerLayout;
    private View rootView;
    private ImageView headerImg;
    private LinearLayoutManager linearLayoutManager;
    private Snackbar snackbar;
    @BindView(R.id.navigationView)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.change_location_layout)
    RelativeLayout change_location_layout;
    @BindView(R.id.location_text)
    TextView locationText;
    @BindView(R.id.no_restaurants_text)
    TextView noRestaurantsText;
    @BindView(R.id.restaurant_recyclerview)
    RecyclerView restaurantRecyclerview;
    @BindView(R.id.cart_button_empty)
    ImageButton cartButton;
    @BindView(R.id.restaurant_layout)
    RelativeLayout restaurantLayout;
    @BindView(R.id.no_restaurants_layout)
    RelativeLayout noRestaurantsLayout;
    @BindView(R.id.cart_view_fill)
    RelativeLayout cartViewFill;
    @BindView(R.id.cart_weight)
    TextView cartWeight;
    @BindView(R.id.cart_total)
    TextView cartTotal;
    @BindView(R.id.coordinator_restaurant)
    CoordinatorLayout coordinateRestaurant;
    @BindView(R.id.swipeToRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.snackbar_layout)
    LinearLayout noOrderSnackbarLayout;
    @BindView(R.id.snackbar_text)
    TextView noOrderSnackbarText;
    private TextView viewProfile;
    private LandingLocation selectedLandingLocation = null;
    private OnRestaurantClickListener onRestaurantClickListener;
    private String selectedLocationStr;
    private Boolean showOrderActivity = false;
    private boolean shouldUserExit = false;
    private CompositeDisposable compositeDisposable;

    @Inject
    public SharedPreferences preferences;

    @Inject
    public ApiClientInterface apiClientInterface;

    @Inject
    public CartSingleton cartSingleton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_restaurant, container, false);
        ButterKnife.bind(this, rootView);
        init();
        fetchProfileData();
        return rootView;
    }

    private void init() {
        context = getContext();
        activity = ((AppCompatActivity) getActivity());
        compositeDisposable = new CompositeDisposable();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(ContextCompat.getDrawable(context, R.drawable.ic_menu));
        toolbar.setNavigationOnClickListener(v -> drawerLayout.openDrawer(GravityCompat.START));
        linearLayoutManager = new LinearLayoutManager(context);
        LandingLocation selectedLandLocation = cartSingleton.getSelectedLandingLocation();
        if (selectedLandLocation == null) {
            selectedLandLocation = cartSingleton.getDefaultLandingLocation();
        }
        selectedLocationStr = selectedLandLocation.getAddress();

        LogUtils.printLog(TAG, "intent =" + activity.getIntent().getExtras());

        if (activity.getIntent().getExtras() != null) {
            selectedLocationStr = activity.getIntent().getStringExtra("selectedLocation");
            showOrderActivity = activity.getIntent().getBooleanExtra("show_order", false);
        }

        selectedLandingLocation = cartSingleton.getSelectedLandingLocation();
        setPickupLocationOnUi(selectedLandingLocation);

        navigationView.setNavigationItemSelectedListener(this);
        headerLayout = navigationView.getHeaderView(0);
        header_userEmail = (TextView) headerLayout.findViewById(R.id.header_userEmail);
        viewProfile = (TextView) headerLayout.findViewById(R.id.view_profile);
        headerImg = (ImageView) headerLayout.findViewById(R.id.headerImg);
        viewProfile.setOnClickListener(v -> {
            drawerLayout.closeDrawers();
            Navigation.findNavController(rootView).navigate(R.id.action_restaurant_to_profileFragment);
        });
        getModeratorStatus();
        restaurantsList = new ArrayList<>();
        if(restaurantsList.size()==0) {
            fetchAllRestaurants();
        }else{
            setupRecyclerView(restaurantsList);
        }
        String userEmail = preferences.getString(StringConstants.KEY_USER_EMAIL, "");
        if (!userEmail.equalsIgnoreCase("")) {
            header_userEmail.setText("Welcome " + userEmail);
        }
        swipeRefreshLayout.setOnRefreshListener(() -> {
            getModeratorStatus();
            fetchAllRestaurants();
        });
    }

    private void getModeratorStatus() {
        Single<Response<ResponseBody>> moderatorObserver = apiClientInterface
                .getModeratorStatus("Bearer " + Utility.getJwtToken());
        moderatorObserver.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getModeratorObservable());
    }

    private SingleObserver<? super Response<ResponseBody>> getModeratorObservable() {
        return new SingleObserver<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(Response<ResponseBody> response) {
                LogUtils.printLog(TAG, "code =" + response.code());
                try {
                    if (response.code() == 200 || response.code() == 201) {
                        if (response.body() != null) {
                            JSONObject moderatorObject = new JSONObject(response.body().string());
                            boolean isOrderAvailable = moderatorObject.getBoolean("is_available");
                            cartSingleton.setOrderAvailable(isOrderAvailable);
                            String orderReason = moderatorObject.getString("reason");
                            cartSingleton.setOrderReasoning(orderReason);
                            setupOrderView();
                        }
                    } else {
                        if(response.code()>=400 && response.code()<=410){
//                            (coordinate_login, context.getResources().getString(R.string.error_email), null, Snackbar.LENGTH_LONG);
                        }else {
                            if (response.errorBody() != null) {
                                JSONObject object = new JSONObject(response.errorBody().string());
                                String message = object.getString("error_description");
//                                showSnackBar(coordinate_login, message, null, Snackbar.LENGTH_LONG);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context.getApplicationContext(),getResources().getString(R.string.error_generic),Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }

    private void setupOrderView() {
        if(!cartSingleton.isOrderAvailable()){
            noOrderSnackbarLayout.setVisibility(View.VISIBLE);
            noOrderSnackbarText.setText(cartSingleton.getOrderReasoning());
        }else{
            noOrderSnackbarLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        if(getArguments()!=null){
//            boolean showOrder = getArguments().getBoolean("show_order");
//            int orderId = getArguments().getInt(StringConstants.KEY_ORDER_ID);
//            LogUtils.printLog(TAG, "showOrder = " + showOrder + " & order_id = " + orderId);
//            getArguments().clear();
//            if(orderId > 0 && showOrder) {
//                Bundle bundle = new Bundle();
//                bundle.putInt(StringConstants.KEY_ORDER_ID, orderId);
//                Navigation.findNavController(rootView).navigate(R.id.action_restaurant_to_OrderStatusFragment, bundle, null, null);
//            }
//        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        onRestaurantClickListener = this;
    }

    //    @Override
//    public void onBackPressed() {
//        drawerLayout.closeDrawers();
//        if (shouldUserExit) {
//            try {
//                this.finish();
//                super.onBackPressed();
//            } catch (Exception e) {
//            }
//        } else {
//            shouldUserExit = true;
//        }
//        showSnackBar(coordinateRestaurant,getResources().getString(R.string.exit_from_app),null, Snackbar.LENGTH_SHORT);
//        new Handler().postDelayed(() -> shouldUserExit = false, 2000);
//    }


    private void fetchProfileData() {
        progressBar.setVisibility(View.VISIBLE);
        Single<UserProfile> profileObservable = apiClientInterface
                .getProfile("Bearer " + Utility.getJwtToken());
        profileObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getProfileObservable());
    }

    private SingleObserver<UserProfile> getProfileObservable() {
        return new SingleObserver<UserProfile>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(UserProfile profile) {
                try {
                    if (profile != null && profile.getBdImage().getThumbnail() != null) {
                        Glide.with(context).load(profile.getBdImage().getThumbnail()).into(headerImg);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(activity.getApplicationContext(), getResources().getString(R.string.error_generic), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        super.onPause();
        compositeDisposable.clear();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Intent intent = null;
        drawerLayout.closeDrawers();
        int id = item.getItemId();
        switch (id) {
            case R.id.browse_neighborhoods:
                Bundle bundle = new Bundle();
                bundle.putBoolean("is_explore", true);
                Navigation.findNavController(rootView).navigate(R.id.action_restaurant_to_mapFragment, bundle, null, null);
                break;
            case R.id.my_orders:
                Navigation.findNavController(rootView).navigate(R.id.action_restaurant_to_orderFragment);
                break;
            case R.id.payments:
                Toast.makeText(context, "Coming Soon...", Toast.LENGTH_LONG).show();
                break;
            case R.id.privacy_policy:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.BYDRONE_URL));
                startActivity(browserIntent);
                break;
            case R.id.help:
                Navigation.findNavController(rootView).navigate(R.id.action_restaurant_to_helpFragment);
                break;
            case R.id.rate_us:
                final String appPackageName = context.getPackageName();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
                break;
            case R.id.feedback:
                Intent feedbackIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.BYDRONE_FEEDBACK_URL));
                startActivity(feedbackIntent);
                break;
            case R.id.logout:
                logout();
                break;
        }
        return true;
    }


    private void logout() {
        Utility.saveJwtToken("");
        preferences.edit().putString(StringConstants.KEY_IS_USER_LOGGED_IN, "").apply();
        preferences.edit().putString(StringConstants.KEY_USER_EMAIL, "").apply();
        cartSingleton.clearCart();
        startActivity(new Intent(activity, SplashActivity.class));
        activity.finish();
    }

    private void fetchAllRestaurants() {
        try {
            if (selectedLandingLocation != null) {
                defaultSelectedLocId = String.valueOf(selectedLandingLocation.getId());
            }

            Single<List<Restaurant>> restaurantObservable = apiClientInterface
                    .restaurantSearchOnLocation("Bearer " + Utility.getJwtToken(), defaultSelectedLocId);
            restaurantObservable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(getRestaurantSubscriber());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SingleObserver<? super List<Restaurant>> getRestaurantSubscriber() {
        return new SingleObserver<List<Restaurant>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(List<Restaurant> restaurants) {
                setupRecyclerView(restaurants);
            }

            @Override
            public void onError(Throwable e) {
                toggleRestaurantVisibility(false);
            }
        };

    }

    private void setupRecyclerView(List<Restaurant> restaurants) {
        restaurantsList = new ArrayList<>();
        restaurantsList.addAll(restaurants);
        if (restaurantsList.size() > 0) {
            restaurantRecyclerview.setLayoutManager(linearLayoutManager);
            adapter = new RestaurantAdapter(restaurantsList, context, onRestaurantClickListener);
            restaurantRecyclerview.setAdapter(adapter);
//                    postponeEnterTransition();
//                    checkTreeObserver();
            toggleRestaurantVisibility(true);
            cartSingleton.getCart().setMaxAllowedCartWeight(0.80);
        } else {
            toggleRestaurantVisibility(false);
        }
    }

    private void checkTreeObserver() {

    }

    private void setupRecyclerViewHeight() {
        if (!cartSingleton.getCart().isEmpty()) {
            float paddingDp = context.getResources().getDimension(R.dimen.size50);
            float density = context.getResources().getDisplayMetrics().density;
            int paddingPixel = (int) (paddingDp * density);
            restaurantRecyclerview.setPadding(0, 0, 0, (int) paddingDp);
        } else {
            restaurantRecyclerview.setPadding(0, 0, 0, 0);
        }
    }

    private void toggleRestaurantVisibility(Boolean shouldShowList) {
        progressBar.setVisibility(View.GONE);
        if (shouldShowList) {
            restaurantLayout.setVisibility(View.VISIBLE);
            noRestaurantsLayout.setVisibility(View.GONE);
        } else {
            restaurantLayout.setVisibility(View.GONE);
            noRestaurantsLayout.setVisibility(View.VISIBLE);
        }

        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

//    private void showSnackbar(final String mainTextString) {
//        Snackbar snackbar = Snackbar.make(coordinateRestaurant, mainTextString, Snackbar.LENGTH_SHORT);
//        View view = snackbar.getView();
//
//        final CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
//        params.setMargins(16, 16, 16, 16);
//
//        view.setBackgroundColor(getResources().getColor(R.color.yellow));
//        TextView tv = (TextView) view.findViewById(com.google.android.material.R.id.snackbar_text);
//        tv.setTextColor(getResources().getColor(R.color.black));
//        snackbar.show();
//    }

    @Override
    public void onResume() {
        super.onResume();
        if (compositeDisposable.isDisposed()) {
            compositeDisposable = new CompositeDisposable();
        }
        selectedLandingLocation = cartSingleton.getSelectedLandingLocation();
        if (selectedLandingLocation == null) {
            selectedLandingLocation = cartSingleton.getDefaultLandingLocation();
        }
        setPickupLocationOnUi(selectedLandingLocation);
        setCartViewData();
    }

    private void setCartViewData() {
        if (cartSingleton.getCart().isEmpty()) {
            cartButton.setVisibility(View.VISIBLE);
            cartViewFill.setVisibility(View.GONE);
        } else {
            cartButton.setVisibility(View.GONE);
            cartViewFill.setVisibility(View.VISIBLE);
            double remainingWeight = cartSingleton.getCart().getRemainingCartWeight();
            String weightStr = getDecimalValue(remainingWeight) + getResources().getString(R.string.kg);
            double price = cartSingleton.getCart().getCartTotal();
            String priceStr = getDecimalValue(price) + getResources().getString(R.string.euro);
            cartTotal.setText(priceStr);
            cartWeight.setText(weightStr);
        }
        setupRecyclerViewHeight();
    }

    private void setPickupLocationOnUi(LandingLocation selectedLandingLocation) {
        if (selectedLandingLocation != null) {
            locationText.setTypeface(null, Typeface.BOLD);
            locationText.setText(selectedLandingLocation.getAddress());

        }
    }

    @OnClick({R.id.location_expand, R.id.location_text})
    void startMapFragment() {
        Navigation.findNavController(rootView).navigate(R.id.action_restaurant_to_mapFragment, null, null, null);
    }


    @OnClick({R.id.cart_button_empty, R.id.cart_view_fill})
    public void openCart() {
        LogUtils.printLog(TAG, "cart " + cartSingleton.getCart().isEmpty());
        Navigation.findNavController(rootView).navigate(R.id.action_restaurant_to_cartFragment);
    }


    @Override
    public void onRestaurantClick(Restaurant restaurant, ImageView imageView) {
        HashMap<View,String> sharedElementsMap = new HashMap<>();
        sharedElementsMap.put(cartViewFill,"cart_transition");
        sharedElementsMap.put(imageView,restaurant.getName());
        cartSingleton.setSelectedRestaurant(restaurant);
        FragmentNavigator.Extras extras = new FragmentNavigator.Extras.Builder()
                .addSharedElements(sharedElementsMap)
                .build();

        Navigation.findNavController(rootView)
                .navigate(R.id.action_restaurant_to_foodItemFragment, null, null, extras);
    }

}
