package com.bydrone.droneapplication.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.bydrone.droneapplication.Cart.CartSingleton;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.StringConstants;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;

/**
 * @author Ankur Khandelwal on 2019-08-23.
 */
public class FinishFragment extends Fragment {
    private String TAG = "FinishFragment";
    private Intent intent = null;
    private int orderId = -1;
    private View rootView;

    @BindView(R.id.order_status_btn)
    Button order_status_btn;
    @BindView(R.id.go_home)
    Button go_home;

    @Inject
    CartSingleton cartSingleton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_finish,container, false);
        ButterKnife.bind(this,rootView);
        cartSingleton.clearCart();
        init();
        return rootView;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    private void init() {
        Bundle bundle = getArguments();
        if(bundle!=null){
            orderId = bundle.getInt(StringConstants.KEY_ORDER_ID);
        }
    }

    @OnClick(R.id.order_status_btn)
    void gotoOrder() {
        Bundle bundle = new Bundle();
        bundle.putInt("orderID",orderId);
        Navigation.findNavController(rootView)
                .navigate(R.id.action_finish_to_status_fragment,bundle,null,null);
    }

    @OnClick(R.id.go_home)
    void goHome(){
        NavOptions navOptions = new NavOptions.Builder()
                .setPopUpTo(R.id.restaurant_fragment, true).build();

        Bundle bundle = new Bundle();
        bundle.putInt("orderID",orderId);
        Navigation.findNavController(rootView)
                .navigate(R.id.action_finish_to_restaurant, bundle, navOptions);

    }
}
