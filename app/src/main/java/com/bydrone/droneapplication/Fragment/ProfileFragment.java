package com.bydrone.droneapplication.Fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.bumptech.glide.Glide;
import com.bydrone.droneapplication.Adapter.CountryListAdapter;
import com.bydrone.droneapplication.Interface.OnCountryItemClickListener;
import com.bydrone.droneapplication.Models.CountryDetails;
import com.bydrone.droneapplication.Models.UserProfile;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.LogUtils;
import com.bydrone.droneapplication.Utils.Utility;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.AndroidSupportInjection;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static android.app.Activity.RESULT_OK;
import static com.bydrone.droneapplication.Utils.Constants.WRITE_EXTERNAL_STORAGE;

public class ProfileFragment extends Fragment implements View.OnFocusChangeListener, OnCountryItemClickListener {
    private static final String TAG = "ProfileFragment";
    private View rootView;
    private Context context;
    private AppCompatActivity activity;
    private static int GALLERY_REQ_CODE = 100;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 0x3;
    private CompositeDisposable compositeDisposable;
    private Uri userProfileUri = null;
    @BindView(R.id.toolbar_profile)
    Toolbar toolbarProfile;
    @BindView(R.id.user_image)
    CircleImageView userImage;
    @BindView(R.id.first_name)
    EditText firstName;
    @BindView(R.id.last_name)
    EditText lastName;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.mobile_number)
    EditText contactNumber;
    @BindView(R.id.save_profile)
    Button saveProfile;
    @BindView(R.id.coordinate_profile)
    CoordinatorLayout coordinateProfile;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.country_code_layout)
    RelativeLayout countryCodeLayout;
    @BindView(R.id.country_code_text)
    TextView countryCodeText;
    @BindView(R.id.bottom_sheet_country_code)
    RelativeLayout bottomSheetCountryCode;
    @BindView(R.id.country_list)
    ListView countryListView;
    @BindView(R.id.close_btn)
    ImageView closeBtn;
    private BottomSheetBehavior bottomSheetBehavior;
    private ArrayList<String> countryCodeList = new ArrayList<>();
    private ArrayList<String> countryList = new ArrayList<>();
    private OnCountryItemClickListener onCountryItemClickListener;

    @Inject
    public SharedPreferences preferences;

    @Inject
    public ApiClientInterface apiClientInterface;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_profile, container, false);
        ButterKnife.bind(this, rootView);
        setupUI();
        return rootView;
    }

    /**
     * First method to call after activity onCreate()
     *
     * @param context
     */
    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        this.context = context;
        this.activity = (AppCompatActivity) getActivity();
    }


    private void setupUI() {
        activity.setSupportActionBar(toolbarProfile);
        activity.getSupportActionBar().setDisplayShowTitleEnabled(false);
        compositeDisposable = new CompositeDisposable();

        email.setEnabled(false);
        contactNumber.setEnabled(false);
        toolbarProfile.setNavigationOnClickListener(v -> {
            hideKeyboard(v);
            Navigation.findNavController(rootView).navigateUp();
        });
        toolbarProfile.setTitle(getResources().getString(R.string.profile));
        toolbarProfile.setTitleTextColor(getResources().getColor(R.color.black));
        toolbarProfile.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black));
        fetchProfileData();

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetCountryCode);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if(newState == BottomSheetBehavior.STATE_DRAGGING){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        firstName.setOnFocusChangeListener(this::onFocusChange);
        lastName.setOnFocusChangeListener(this::onFocusChange);
        email.setOnFocusChangeListener(this::onFocusChange);
        contactNumber.setOnFocusChangeListener(this::onFocusChange);
        getCountryDetails();
    }

    private void getCountryDetails() {
        countryCodeList = new ArrayList<>(Arrays.asList(CountryDetails.getCode()));
        countryList = new ArrayList<>(Arrays.asList(CountryDetails.getCountry()));
    }

//    @OnClick(R.id.country_code_layout)
    void openCountrySelectBottomSheet(){
        showBottomSheet();
        CountryListAdapter adapter = new CountryListAdapter(context, countryList,countryCodeList,onCountryItemClickListener);
        countryListView.setAdapter(adapter);
    }

    private void showBottomSheet(){
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @OnClick(R.id.close_btn)
    void hideBottomSheet(){
        if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }


    private void fetchProfileData() {
        Single<UserProfile> profileObservable = apiClientInterface
                .getProfile("Bearer " + Utility.getJwtToken());
        profileObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fetchProfileObservable());
    }

    private SingleObserver<UserProfile> fetchProfileObservable() {
        return new SingleObserver<UserProfile>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(UserProfile profile) {
                try {
                    if (profile != null) {
                        setupProfile(profile);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(activity.getApplicationContext(), getResources().getString(R.string.error_generic), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Throwable e) {

            }
        };
    }

    private void setupProfile(UserProfile profile) {
        if(profile!=null) {
            firstName.setText(profile.getFirstName());
            lastName.setText(profile.getLastName());
            email.setText(profile.getEmail());
            contactNumber.setText(profile.getContactNumber());
            Glide.with(context).load(profile.getBdImage().getFullSize()).into(userImage);
        }
    }

    public void showSnackBar(View coordinateView, String s, String actionString, int snackbarDuration) {
        Snackbar snackbar = Snackbar.make(coordinateView, s, snackbarDuration);
        View view = snackbar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.colorAccent));

        final CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
        params.setMargins(16, 16, 16, 16);

        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(getResources().getColor(R.color.white));
        snackbar.show();
    }

    private void hideKeyboard(View view) {
        hideBottomSheet();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



    @OnClick(R.id.save_profile)
    public void sendProfileToServer() {
        try {
            if (firstName.getText().length() > 0 && lastName.getText().length() > 0 && email.getText().length() > 0 && contactNumber.getText().length() > 0) {
                String userContact = contactNumber.getText().toString();
                String firstNameStr = firstName.getText().toString();
                String lastNameStr = lastName.getText().toString();
                String emailStr = email.getText().toString();
                String contactStr = contactNumber.getText().toString();

//                HashMap<String, String> userHashMap = new HashMap<>();
//                userHashMap.put("first_name", firstName.getText().toString());
//                userHashMap.put("last_name", lastName.getText().toString());
//                userHashMap.put("phone_number", "+358" + userContact);
//                userHashMap.put("email", email.getText().toString());

                MultipartBody.Part userImagePart = null;
                if (userProfileUri != null) {
                    LogUtils.printLog(TAG, " userprofileUri =" + userProfileUri);
                    String fileName = Utility.getFileName(activity, userProfileUri);
                    if (fileName != null) {
                        LogUtils.printLog(TAG, " fileName =" + fileName);
                        File file = new File(fileName);
                        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                        userImagePart = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
                    }
                }

                RequestBody firstNameBody = RequestBody.create(MediaType.parse("text/plain"), firstNameStr);
                RequestBody lastNameBody = RequestBody.create(MediaType.parse("text/plain"), lastNameStr);
                RequestBody emailBody = RequestBody.create(MediaType.parse("text/plain"), emailStr);
                RequestBody contactNumberBody = RequestBody.create(MediaType.parse("text/plain"), contactStr);

                LogUtils.printLog(TAG, "  userImagePart = " + userImagePart);
                Single<UserProfile> postProfileObservable = apiClientInterface.postProfile("Bearer " + Utility.getJwtToken(),
                        firstNameBody, lastNameBody, emailBody, contactNumberBody, userImagePart);

                toggleUI(true);
                postProfileObservable.subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(getPostProfileObserver());
            } else {
                showSnackBar(coordinateProfile, context.getResources().getString(R.string.error_enter_all_fields), null, Snackbar.LENGTH_LONG);
            }
        } catch (Exception e) {
            LogUtils.printLog(TAG, "error =" + e.getMessage());
        }
    }

    private void toggleUI(boolean showProgressbar) {
        if (showProgressbar) {
            progressBar.setVisibility(View.VISIBLE);
            saveProfile.setVisibility(View.GONE);
        }else{
            progressBar.setVisibility(View.GONE);
            saveProfile.setVisibility(View.VISIBLE);
        }
    }

    private SingleObserver<UserProfile> getPostProfileObserver() {
        return new SingleObserver<UserProfile>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(UserProfile userProfile) {
                toggleUI(false);
                try {
                    if (userProfile != null) {
                        showSnackBar(coordinateProfile, context.getResources().getString(R.string.profile_saved), null, Snackbar.LENGTH_LONG);
                        setupProfile(userProfile);
                    } else {
                        showSnackBar(coordinateProfile, context.getResources().getString(R.string.error_generic), null, Snackbar.LENGTH_LONG);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                toggleUI(false);
                LogUtils.printLog(TAG, "error = " + e.getMessage());
                showSnackBar(coordinateProfile, getResources().getString(R.string.error_generic), null, Snackbar.LENGTH_LONG);
            }
        };
    }

    @Override
    public void onPause() {
        super.onPause();
        compositeDisposable.clear();
        toggleUI(false);
    }


    @OnClick({R.id.user_image, R.id.edit_image})
    public void editImage() {
        LogUtils.printLog(TAG, "image onClick");
        if (checkPermission()) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, getResources().getString(R.string.select_picture)), GALLERY_REQ_CODE);
        } else {
            requestPermission();
        }
    }

    private void requestPermission() {
        Log.i(TAG, "Displaying permission rationale to provide additional context.");
        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private boolean checkPermission() {
        int permissionState = context.checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Permission granted
                editImage();
            } else {
                LogUtils.printLog(TAG, "Permission result -> Not granted");
                //permission not granted.
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                if (data != null && data.getData() != null) {
                    userProfileUri = data.getData();
                    final InputStream imageStream = activity.getContentResolver().openInputStream(userProfileUri);
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    LogUtils.printLog(TAG, "userImageuri = " + userProfileUri);
                    userImage.setImageBitmap(selectedImage);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        hideBottomSheet();
    }

    @Override
    public void onItemClick(String countryName, String countryCode) {
        hideBottomSheet();
        countryCodeText.setText(countryCode);
    }
}
