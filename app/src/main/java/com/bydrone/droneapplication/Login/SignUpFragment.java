package com.bydrone.droneapplication.Login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bydrone.droneapplication.Adapter.CountryListAdapter;
import com.bydrone.droneapplication.BaseActivity;
import com.bydrone.droneapplication.Interface.OnCountryItemClickListener;
import com.bydrone.droneapplication.Models.CountryDetails;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Utils.LogUtils;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class SignUpFragment extends Fragment implements OnCountryItemClickListener, View.OnFocusChangeListener {
    String TAG = "SignUpFragment";

    @BindView(R.id.registerButton)
    Button registerButton;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinate_signup)
    CoordinatorLayout coordinateSignup;

    @BindView(R.id.first_name)
    EditText userFirstNameField;
    @BindView(R.id.last_name)
    EditText userLastNameField;
    @BindView(R.id.email)
    EditText userEmailField;
    @BindView(R.id.password)
    EditText userPasswordField;
    @BindView(R.id.rePassword)
    EditText userConfirmPasswordField;
    @BindView(R.id.mobile_number)
    EditText userContactNumberField;
    @BindView(R.id.toolbar_signup)
    Toolbar toolbar;
    @BindView(R.id.country_code_layout)
    RelativeLayout countryCodeLayout;
    @BindView(R.id.country_code_text)
    TextView countryCodeText;
    @BindView(R.id.bottom_sheet_country_code)
    RelativeLayout bottomSheetCountryCode;
    @BindView(R.id.country_list)
    ListView countryListView;
    @BindView(R.id.close_btn)
    ImageView closeBtn;

    private CompositeDisposable compositeDisposable;
    private Context context;
    private AppCompatActivity activity;
    private View rootView;
    private BottomSheetBehavior bottomSheetBehavior;
    private ArrayList<String> countryCodeList = new ArrayList<>();
    private ArrayList<String> countryList = new ArrayList<>();
    @Inject
    public SharedPreferences preferences;

    @Inject
    public ApiClientInterface apiClientInterface;
    private OnCountryItemClickListener onCountryItemClickListener;
    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        this.context = context;
        this.activity = ((AppCompatActivity) getActivity());
        onCountryItemClickListener = this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_signup,container,false);
        ButterKnife.bind(this,rootView);
        init();
        return rootView;
    }

    private void init() {
        toolbar.setTitle(getResources().getString(R.string.register));
        toolbar.setTitleTextColor(ContextCompat.getColor(context, R.color.black));
        toolbar.setNavigationIcon(ContextCompat.getDrawable(context, R.drawable.ic_arrow_back_black));
        toolbar.setNavigationOnClickListener(v -> Navigation.findNavController(rootView).navigateUp());
        compositeDisposable = new CompositeDisposable();

        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetCountryCode);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if(newState == BottomSheetBehavior.STATE_DRAGGING){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        userFirstNameField.setOnFocusChangeListener(this::onFocusChange);
        userLastNameField.setOnFocusChangeListener(this::onFocusChange);
        userEmailField.setOnFocusChangeListener(this::onFocusChange);
        userContactNumberField.setOnFocusChangeListener(this::onFocusChange);
        userPasswordField.setOnFocusChangeListener(this::onFocusChange);

        getCountryDetails();
        showSnackBar(coordinateSignup,getResources().getString(R.string.email_verification_info), Snackbar.LENGTH_LONG);
    }

    private void getCountryDetails() {
        countryCodeList = new ArrayList<>(Arrays.asList(CountryDetails.getCode()));
        countryList = new ArrayList<>(Arrays.asList(CountryDetails.getCountry()));
    }

    @OnClick(R.id.country_code_layout)
    void openCountrySelectBottomSheet(){
        showBottomSheet();
        hideKeyboard(rootView);
        CountryListAdapter adapter = new CountryListAdapter(context, countryList,countryCodeList,onCountryItemClickListener);
        countryListView.setAdapter(adapter);
    }

    private void showBottomSheet(){
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @OnClick(R.id.close_btn)
    void hideBottomSheet(){
        if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @OnClick(R.id.registerButton)
    void performSignIn(View v) {
        hideKeyboard(v);
        String mUserFirstName = userFirstNameField.getText().toString();
        String mUserLastName = userLastNameField.getText().toString();
        String mUserEmail = userEmailField.getText().toString();
        String mUserPassword = userPasswordField.getText().toString();
        String mUserContactNumber = userContactNumberField.getText().toString();

        if (mUserFirstName.length() == 0 || mUserLastName.length() == 0 || mUserEmail.length() == 0 || mUserPassword.length() == 0 || mUserContactNumber.length() == 0) {
            showSnackBar(coordinateSignup,getResources().getString(R.string.error_all_fields),Snackbar.LENGTH_LONG);
            return;
        }
        if (!mUserEmail.contains("@") || mUserEmail.indexOf('@') >= mUserEmail.lastIndexOf('.')) {
            showSnackBar(coordinateSignup,getResources().getString(R.string.error_email), Snackbar.LENGTH_LONG);
            return;
        }
        if (mUserPassword.length() < 4) {
            showSnackBar(coordinateSignup, getResources().getString(R.string.error_password), Snackbar.LENGTH_LONG);
            return;
        }

        postSignupForm(mUserFirstName, mUserLastName, mUserContactNumber, mUserEmail, mUserPassword);
    }

    private void hideKeyboard(View view) {
        hideBottomSheet();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private void showSnackBar(View coordinateSignup, String s, int duration) {
        Snackbar snackbar = Snackbar.make(coordinateSignup, s, duration);
        View view = snackbar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.yellow));
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(getResources().getColor(R.color.black));
        snackbar.show();
    }

    private void postSignupForm(String mUserFirstName, String mUserLastName, String mUserContactNumber, String mUserEmail, String mUserPassword) {
        progressBar.setVisibility(View.VISIBLE);
        String contact = countryCodeText.getText().toString().concat(mUserContactNumber);
        contact = contact.replace(" ","");

        HashMap<String, String> userHashMap = new HashMap<>();
        userHashMap.put("first_name", mUserFirstName);
        userHashMap.put("last_name", mUserLastName);
        userHashMap.put("phone_number", contact);
        userHashMap.put("email", mUserEmail);
        userHashMap.put("password", mUserPassword);


        Single<Response<ResponseBody>> signupObservable = apiClientInterface.register(userHashMap);
        signupObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSignupObserver());
    }



    private SingleObserver<? super Response<ResponseBody>> getSignupObserver() {
        return new SingleObserver<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(Response<ResponseBody> response) {
                progressBar.setVisibility(View.GONE);
                LogUtils.printLog(TAG, "code =" + response.code());
                try {
                    if (response.code() == 200 || response.code() == 201) {
                        if (response.body() != null) {
                            showSnackBar(coordinateSignup,getResources().getString(R.string.email_verification_info),Snackbar.LENGTH_LONG);
                            Toast.makeText(context.getApplicationContext(), "Signup success", Toast.LENGTH_LONG).show();
                            Navigation.findNavController(rootView).navigateUp();
                        }
                    } else {
                        if (response.errorBody() != null) {
                            JSONObject errorObj = new JSONObject(response.errorBody().string());
                            LogUtils.printLog(TAG, "error obj = " + errorObj);
                            if (errorObj.has("email")) {
                                showSnackBar(coordinateSignup,errorObj.getString("email"),Snackbar.LENGTH_LONG);
                            }else if(errorObj.has("phone_number")){
                                showSnackBar(coordinateSignup,errorObj.getString("phone_number"),Snackbar.LENGTH_LONG);
                            }
                            else {
                                showSnackBar(coordinateSignup,getResources().getString(R.string.error_user_exist),Snackbar.LENGTH_LONG);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                progressBar.setVisibility(View.GONE);
                showSnackBar(coordinateSignup,getResources().getString(R.string.error_generic),Snackbar.LENGTH_LONG);
            }
        };
    }

    @Override
    public void onPause() {
        super.onPause();
        compositeDisposable.clear();
    }

    @Override
    public void onItemClick(String countryName, String countryCode) {
        hideBottomSheet();
        countryCodeText.setText(countryCode);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        hideBottomSheet();
    }
}
