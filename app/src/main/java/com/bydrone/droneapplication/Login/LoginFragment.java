package com.bydrone.droneapplication.Login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.bydrone.droneapplication.MainActivity;
import com.bydrone.droneapplication.Networking.ApiClientInterface;
import com.bydrone.droneapplication.R;
import com.bydrone.droneapplication.Splash.SplashActivity;
import com.bydrone.droneapplication.Utils.LogUtils;
import com.bydrone.droneapplication.Utils.StringConstants;
import com.bydrone.droneapplication.Utils.Utility;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class LoginFragment extends Fragment {
    private String TAG = "LoginFragment";
    private String mUserName, mUserPassword;
    private CompositeDisposable compositeDisposable;
    private BottomSheetBehavior bottomSheetBehavior;
    private Context context;
    private AppCompatActivity activity;
    private View rootView;
    @BindView(R.id.signin_button)
    Button signin_button;
    @BindView(R.id.registerButton)
    TextView registerButton;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.coordinate_login)
    CoordinatorLayout coordinate_login;
    @BindView(R.id.bottom_sheet_forgot_pass)
    RelativeLayout bottomSheetForgotPassword;
    @BindView(R.id.send_forgot_password_btn)
    TextView forgotPasswordBtn;
    @BindView(R.id.email_forgot_password)
    TextInputEditText emailForgotPassword;
    @BindView(R.id.progress_forgotpassword)
    ProgressBar progressForgotPassword;
    @BindView(R.id.forgot_pass_layout)
    RelativeLayout forgotPasswordLayout;
    @Inject
    SharedPreferences preferences;
    @Inject
    ApiClientInterface apiClientInterface;

    @Override
    public void onAttach(@NonNull Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
        this.context = context;
        this.activity = ((AppCompatActivity) getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_login,container, false);
        ButterKnife.bind(this,rootView);
        setupUI();
        return rootView;
    }

    private void setupUI() {
        compositeDisposable = new CompositeDisposable();
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetForgotPassword);
        bottomSheetBehavior.setPeekHeight(0);
    }

    @OnClick(R.id.registerButton)
    void startSignupActivity() {
        Navigation.findNavController(rootView).navigate(R.id.action_login_to_singupFragment);
    }

    @OnClick(R.id.signin_button)
    void doLogin(View v) {
        hideKeyboard(v);
        EditText userEmailField = rootView.findViewById(R.id.signin_email);
        EditText userPasswordField = rootView.findViewById(R.id.sigin_password);
        mUserName = userEmailField.getText().toString();
        mUserPassword = userPasswordField.getText().toString();

        if (mUserName.length() == 0) {
            showSnackBar(coordinate_login, getResources().getString(R.string.email_info_error), null, Snackbar.LENGTH_LONG);
        } else if (mUserPassword.length() == 0) {
            showSnackBar(coordinate_login, getResources().getString(R.string.password_info_error), null, Snackbar.LENGTH_LONG);
        } else {
            checkLoginUsername(mUserName, mUserPassword);
        }
    }

    @OnClick(R.id.forgetPasswordText)
    void showBottomSheet(){
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    @OnClick(R.id.bottom_sheet_forgot_pass)
    void hideBottomSheet(){
        if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED){
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @OnClick(R.id.send_forgot_password_btn)
    void onForgotPasswordRequest(){
        if(emailForgotPassword.getText().length() > 0) {
            String mUserEmail  = emailForgotPassword.getText().toString();
            if (!mUserEmail.contains("@") || mUserEmail.indexOf('@') >= mUserEmail.lastIndexOf('.')) {
                showSnackBar(coordinate_login,getResources().getString(R.string.error_email), null, Snackbar.LENGTH_LONG);
                return;
            }else{
                postForgotPasswordRequest(mUserEmail);
            }
        }else{
            showSnackBar(coordinate_login, getResources().getString(R.string.email_info_error), null, Snackbar.LENGTH_LONG);
        }
    }

    private void postForgotPasswordRequest(String mUserEmail) {
        progressForgotPassword.setVisibility(View.VISIBLE);
        forgotPasswordLayout.setVisibility(View.INVISIBLE);
        HashMap<String,String> passwordMap = new HashMap<>();
        passwordMap.put("email",mUserEmail);
        Single<Response<ResponseBody>> forgotPasswordObservable = apiClientInterface.forgotPassword(passwordMap);
        forgotPasswordObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getForgotPasswordObservable());

    }

    private SingleObserver<? super Response<ResponseBody>> getForgotPasswordObservable() {
        return new SingleObserver<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(Response<ResponseBody> response) {
                progressForgotPassword.setVisibility(View.GONE);
                forgotPasswordLayout.setVisibility(View.VISIBLE);
                LogUtils.printLog(TAG, "code =" + response.code());
                try {
                    if (response.code() == 200 || response.code() == 201) {
                        if (response.body() != null) {
                            showSnackBar(coordinate_login,getResources().getString(R.string.forgot_password_send_message),null,Snackbar.LENGTH_LONG);
                            dissmissBottomSheet();
                        }
                    } else {
                        if(response.code()>=400 && response.code()<=410){
                            showSnackBar(coordinate_login, context.getResources().getString(R.string.error_email), null, Snackbar.LENGTH_LONG);
                        }else {
                            if (response.errorBody() != null) {
                                JSONObject object = new JSONObject(response.errorBody().string());
                                String message = object.getString("error_description");
                                showSnackBar(coordinate_login, message, null, Snackbar.LENGTH_LONG);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context.getApplicationContext(),getResources().getString(R.string.error_generic),Toast.LENGTH_LONG).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                progressForgotPassword.setVisibility(View.GONE);
                forgotPasswordLayout.setVisibility(View.VISIBLE);
                Toast.makeText(context.getApplicationContext(),getResources().getString(R.string.error_generic),Toast.LENGTH_LONG).show();
            }
        };
    }

    @OnClick({R.id.back_arrow})
    public void dissmissBottomSheet(){
        hideKeyboard();
        if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    private void hideKeyboard() {
        View v = activity.getWindow().getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    private void checkLoginUsername(final String mUserName, final String mUserPassword) {
        progressBar.setVisibility(View.VISIBLE);

        Single<Response<ResponseBody>> loginObservable = apiClientInterface.login("password", "mobile_app_login", mUserName, mUserPassword);
        loginObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(getSingleLoginObserver());

    }

    private SingleObserver<? super Response<ResponseBody>> getSingleLoginObserver() {
        return new SingleObserver<Response<ResponseBody>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onSuccess(Response<ResponseBody> response) {
                progressBar.setVisibility(View.GONE);
                LogUtils.printLog(TAG, "code =" + response.code());
                try {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            JSONObject object = new JSONObject(response.body().string());
                            String access_token = object.getString("access_token");
                            LogUtils.printLog(TAG, "access_token =" + access_token);
                            saveData(mUserName, mUserPassword, access_token);
                        }
                    } else {
                        if (response.errorBody() != null) {
                            JSONObject object = new JSONObject(response.errorBody().string());
                            String message = object.getString("error_description");
                            showSnackBar(coordinate_login, message, null, Snackbar.LENGTH_LONG);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Throwable e) {
                progressBar.setVisibility(View.GONE);
                showSnackBar(coordinate_login,getResources().getString(R.string.error_generic),null,Snackbar.LENGTH_LONG);
            }
        };
    }

    private void showSnackBar(CoordinatorLayout coordinate_login, String string, Object o, int lengthLong) {
        Snackbar snackbar = Snackbar.make(coordinate_login, string, lengthLong);
        View view = snackbar.getView();
        view.setBackgroundColor(getResources().getColor(R.color.yellow));
        TextView tv = view.findViewById(com.google.android.material.R.id.snackbar_text);
        tv.setTextColor(getResources().getColor(R.color.black));
        snackbar.show();
    }

    private void saveData(String mUserEmail, String mUserPassword, String accessToken) {
        preferences.edit().putString(StringConstants.KEY_IS_USER_LOGGED_IN, "true").apply();
        preferences.edit().putString(StringConstants.KEY_USER_EMAIL, mUserEmail).apply();
        preferences.edit().putBoolean(StringConstants.KEY_SHOULD_SHOW_MAP,true).apply();
        Utility.saveJwtToken(accessToken);
        startHomeActivity();
    }

    private void startHomeActivity() {
        startActivity(new Intent(activity, MainActivity.class));
        activity.finish();
//        Navigation.findNavController(rootView).navigate(R.id.action_login_to_mainActivity);
    }

    private void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void onPause() {
        super.onPause();
        compositeDisposable.clear();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
