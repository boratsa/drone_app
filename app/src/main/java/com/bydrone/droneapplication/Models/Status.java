package com.bydrone.droneapplication.Models;

/**
 * @author Ankur Khandelwal on 2019-08-16.
 */
public class Status {
    private int status;
    private String status_text;
    private String time;
    private boolean isPending = true;

    public boolean getIsPending() {
        return isPending;
    }

    public void setPending(boolean pending) {
        isPending = pending;
    }

    public Status(int status_data, String status_text_data, String time_data, boolean isPending) {
        this.status = status_data;
        this.status_text = status_text_data;
        this.time = time_data;
        this.isPending = isPending;
    }

    public int getStatus() {
        return status;
    }

    public String getStatus_text() {
        return status_text;
    }

    public String getTime() {
        return time;
    }
}
