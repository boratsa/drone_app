package com.bydrone.droneapplication.Models;

public class LandingLocation {
    private int id;
    private double mLatitude = 0.0;
    private double mLongitude = 0.0;
    private String address = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getmLatitude() {
        return mLatitude;
    }

    public void setmLatitude(double mLatitude) {
        this.mLatitude = mLatitude;
    }

    public double getmLongitude() {
        return mLongitude;
    }

    public void setmLongitude(double mLongitude) {
        this.mLongitude = mLongitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    private String distance = "";

    public LandingLocation(int id,double mLatitude, double mLongitude, String address, String distance) {
        this.id=id;
        this.mLatitude = mLatitude;
        this.mLongitude = mLongitude;
        this.address = address;
        this.distance = distance;
    }


}
