package com.bydrone.droneapplication.Models;

public class FoodItem {
    private int id;
    private String name;
    private String price;
    private String weight;
    private Restaurant restaurant;
    private int restaurantId;
    private String restaurantName;
    private int quantity = 0;
    private int maxQuantity=10;
    private String foodType;
    private int itemType = 1;
    private String imageUrl = "https://tsa-ddrop.s3.amazonaws.com/media/__sized__/restaurants/defaults/1-thumbnail-100x100.png";
    private String imageUrlSmall;
    private String imageUrlMedium;
    private String imageUrlFull;
    private String imageThumbnail;
    private boolean isAvailable;

    public FoodItem(String item_name, String item_weight, String item_price, int item_id, Restaurant restaurant,
                    String foodType, String imageUrlSmall, String imageUrlMedium, String imageUrlFull,
                    String imageThumbnail, boolean isAvailable) {
        this.name = item_name;
        this.weight = item_weight;
        this.price = item_price;
        this.id = item_id;
        this.restaurant = restaurant;
        this.foodType = foodType;
        this.imageUrlSmall = imageUrlSmall;
        this.imageUrlMedium = imageUrlMedium;
        this.imageUrlFull = imageUrlFull;
        this.imageThumbnail = imageThumbnail;
        this.isAvailable = isAvailable;
    }

    public FoodItem(String item_name, String item_weight, String item_price, int item_id, Restaurant restaurant, int quantity,
                    String foodType,String imageUrlSmall, String imageUrlMedium, String imageUrlFull,
                    String imageThumbnail) {
        this.name = item_name;
        this.weight = item_weight;
        this.price = item_price;
        this.id = item_id;
        this.restaurant = restaurant;
        this.quantity = quantity;
        this.foodType = foodType;
        this.imageUrlSmall = imageUrlSmall;
        this.imageUrlMedium = imageUrlMedium;
        this.imageUrlFull = imageUrlFull;
        this.imageThumbnail = imageThumbnail;
    }

    public FoodItem(int quantity, String weight, String name) {
        this.quantity = quantity;
        this.weight = weight;
        this.name = name;
    }

    public int getMaxQuantity() {
        return maxQuantity;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public String getImageUrlSmall() {
        return imageUrlSmall;
    }

    public void setImageUrlSmall(String imageUrlSmall) {
        this.imageUrlSmall = imageUrlSmall;
    }

    public String getImageUrlMedium() {
        return imageUrlMedium;
    }

    public void setImageUrlMedium(String imageUrlMedium) {
        this.imageUrlMedium = imageUrlMedium;
    }

    public String getImageUrlFull() {
        return imageUrlFull;
    }

    public void setImageUrlFull(String imageUrlFull) {
        this.imageUrlFull = imageUrlFull;
    }

    public String getImageThumbnail() {
        return imageThumbnail;
    }

    public void setImageThumbnail(String imageThumbnail) {
        this.imageThumbnail = imageThumbnail;
    }


    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }


    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }


    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }

    public String getWeight() {
        return weight;
    }

    public int getRestaurantId() {
        return restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isMaxQuantityReached(){
        return quantity >= maxQuantity;
    }

    @Override
    public String toString() {
        return "id=" + this.id + " &qunatity= " + this.quantity;
    }
}
