package com.bydrone.droneapplication.Models;

import android.util.Log;

import com.bydrone.droneapplication.Utils.LogUtils;
import com.google.android.material.tabs.TabLayout;

import java.math.BigDecimal;
import java.util.ArrayList;

import butterknife.OnClick;

/**
 * @author Ankur Khandelwal on 2019-07-22.
 */
public class Cart {
    private ArrayList<FoodItem> foodItems = new ArrayList<>();
    private double cartTotal = 0.0;
    private double cartWeight = 0.0;
    private double maxAllowedCartWeight;
    private static final float PRECISION_LEVEL = 0.000f;
    public ArrayList<FoodItem> getFoodItems() {
        return this.foodItems;
    }

    public void setFoodItems(ArrayList<FoodItem> foodItems) {
        this.foodItems = foodItems;
    }

    public double getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(double cartTotal) {
        this.cartTotal = cartTotal;
    }

    public double getCartWeight() {
        return cartWeight;
    }

    public void setCartWeight(double cartWeight) {
        this.cartWeight = cartWeight;
    }

    public void updateFoodItem(FoodItem foodItem, int itemCount) {
        boolean isExist = false;
        if (itemCount == 0) {
            LogUtils.printLog("Cart Model itemCount ",String.valueOf(itemCount));
            FoodItem item = getFoodItem(foodItem.getId());
            if(item!=null){
                this.foodItems.remove(item);
            }
        } else {
            for (int i = 0; i < this.foodItems.size(); i++) {
                if (this.foodItems.get(i).getId() == foodItem.getId()) {
                    isExist = true;
                    LogUtils.printLog("addingFoodItem", String.valueOf(true));
                    this.foodItems.get(i).setQuantity(itemCount);
                }
            }
            if (!isExist) {
                foodItem.setQuantity(itemCount);
                this.foodItems.add(foodItem);
            }
        }
        this.setFoodItems(foodItems);
        updateCartTotal();
        updateCartWeight();

        LogUtils.printLog("after update fooditems" , String.valueOf(foodItems));
    }

    private void updateCartTotal() {
        double total = 0;
        for(FoodItem foodItem : foodItems){
            total+=(foodItem.getQuantity() * Double.valueOf(foodItem.getPrice()));
        }
        this.cartTotal = total;
    }

    private void updateCartWeight() {
        double totalWeight = 0.0;
        for(FoodItem foodItem : foodItems){
            totalWeight +=(foodItem.getQuantity() * Double.valueOf(foodItem.getWeight()));
        }
        this.cartWeight = totalWeight/1000;
    }

    public int getFoodItemQuantity(int id) {
        int quantity = 0;
        FoodItem item = getFoodItem(id);
        if(item!=null){
            return item.getQuantity();
        }
        return quantity;
    }

    public FoodItem getFoodItem(int id) {
        FoodItem foodItem = null;
        for (int i = 0; i < foodItems.size(); i++) {
            if (foodItems.get(i).getId() == id) {
                foodItem = foodItems.get(i);
            }
        }
        return foodItem;
    }

    public boolean isRestaurantChange(FoodItem foodItem){
        boolean isChange = false;
        if(foodItems.size()>0){
            if(foodItems.get(0).getRestaurant().getId() != foodItem.getRestaurant().getId()){
                isChange = true;
            }
        }
        return isChange;
    }

    public void clearCart() {
        this.foodItems.clear();
        this.cartTotal = 0.0;
        this.cartWeight = 0.0;
    }

    public void printItems() {
        for (int i = 0; i < this.foodItems.size(); i++) {
            LogUtils.printLog("CartItem", this.foodItems.get(i).toString());
        }
    }

    public boolean isEmpty() {
        return this.foodItems.size() == 0;
    }

    public double getMaxAllowedCartWeight() {
        return maxAllowedCartWeight;
    }

    public void setMaxAllowedCartWeight(double maxAllowedCartWeight) {
        this.maxAllowedCartWeight = maxAllowedCartWeight;
    }

    public boolean cartMaxWeightReached(FoodItem foodItem,int itemCount){
        float maxCartWeight = (float) getMaxAllowedCartWeight();
        float foodItemWeight = (float) (Double.valueOf(foodItem.getWeight()) / 1000);
        float updatedCartWeight = (float) cartWeight + (foodItemWeight);
        float difference = maxCartWeight - updatedCartWeight;
        return !(difference >= PRECISION_LEVEL);
    }

    public double getRemainingCartWeight(){
        return maxAllowedCartWeight - cartWeight;
    }
}
