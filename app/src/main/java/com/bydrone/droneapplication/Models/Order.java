package com.bydrone.droneapplication.Models;

import androidx.annotation.StringRes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Order {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("src")
    @Expose
    private LocationPlaces sourceLocation;
    @SerializedName("dest")
    @Expose
    private LocationPlaces destinationLocation;
    @SerializedName("goods")
    @Expose
    private Goods goods;
    @SerializedName("weight")
    @Expose
    private String total_item_weight;
    @SerializedName("status")
    @Expose
    private int status;
    @SerializedName("invoice")
    @Expose
    private String invoiceUrl;
    @SerializedName("created")
    @Expose
    private String date;
    @SerializedName("amount")
    @Expose
    private String amount;


    public Goods getGoods() {
        return goods;
    }

    public void setGoods(Goods goods) {
        this.goods = goods;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public LocationPlaces getSourceLocation() {
        return sourceLocation;
    }

    public void setSourceLocation(LocationPlaces sourceLocation) {
        this.sourceLocation = sourceLocation;
    }

    public LocationPlaces getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(LocationPlaces destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public String getTotal_item_weight() {
        return total_item_weight;
    }

    public void setTotal_item_weight(String total_item_weight) {
        this.total_item_weight = total_item_weight;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getInvoiceUrl() {
        return invoiceUrl;
    }

    public void setInvoiceUrl(String invoiceUrl) {
        this.invoiceUrl = invoiceUrl;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}