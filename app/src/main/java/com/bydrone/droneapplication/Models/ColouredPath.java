package com.bydrone.droneapplication.Models;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

/**
 * @author Ankur Khandelwal on 19/04/18.
 */

public class ColouredPath {
    public ArrayList<LatLng> points;
    public int index;

    public ColouredPath(ArrayList<LatLng> points, int index) {
        this.points = points;
        this.index = index;
    }

}
