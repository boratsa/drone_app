package com.bydrone.droneapplication.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @author Ankur Khandelwal on 2019-08-18.
 */
public class Goods {
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("items")
    @Expose
    private ArrayList<FoodItem> items;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<FoodItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<FoodItem> items) {
        this.items = items;
    }
}
