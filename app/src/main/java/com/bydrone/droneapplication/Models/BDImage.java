package com.bydrone.droneapplication.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author Ankur Khandelwal on 2019-08-18.
 */
public class BDImage {
    @SerializedName("small_square_crop")
    @Expose
    private String smallSquareCrop;
    @SerializedName("medium_square_crop")
    @Expose
    private String mediumSquareCrop;
    @SerializedName("full_size")
    @Expose
    private String fullSize;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    public String getSmallSquareCrop() {
        return smallSquareCrop;
    }

    public void setSmallSquareCrop(String smallSquareCrop) {
        this.smallSquareCrop = smallSquareCrop;
    }

    public String getMediumSquareCrop() {
        return mediumSquareCrop;
    }

    public void setMediumSquareCrop(String mediumSquareCrop) {
        this.mediumSquareCrop = mediumSquareCrop;
    }

    public String getFullSize() {
        return fullSize;
    }

    public void setFullSize(String fullSize) {
        this.fullSize = fullSize;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
