package com.bydrone.droneapplication.Models;

import java.util.ArrayList;

/**
 * @author Ankur Khandelwal on 2019-07-30.
 */
public class Category {
    private int id;
    private String name;
    private ArrayList<FoodItem> items;
    private int itemType = 0;

    public int getItemType() {
        return itemType;
    }

    public void setItemType(int itemType) {
        this.itemType = itemType;
    }

    public Category(int id, String name, ArrayList<FoodItem> items) {
        this.id = id;
        this.name = name;
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<FoodItem> getItems() {
        return items;
    }

    public void setItems(ArrayList<FoodItem> items) {
        this.items = items;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


}
